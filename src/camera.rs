use crate::canvas::Canvas;
use crate::d3::Point3D;
use crate::matrix::Matrix4By4;
use crate::ray::Ray;
use crate::world::World;

pub struct Camera {
    pub hsize: u32,
    pub vsize: u32,
    pub field_of_view: f64,
    pub transform: Matrix4By4,
    pub pixel_size: f64,
    pub half_width: f64,
    pub half_height: f64,
}

impl Camera {
    pub fn new(hsize: u32, vsize: u32, field_of_view: f64) -> Self {
        // compute pixel size
        let half_view = (field_of_view / 2.0).tan();
        let aspect = hsize as f64 / vsize as f64;

        let (half_width, half_height) = if hsize >= vsize {
            (half_view, half_view / aspect)
        } else {
            (half_view * aspect, half_view)
        };

        Self {
            hsize,
            vsize,
            field_of_view,
            transform: Matrix4By4::identity(),
            pixel_size: half_width * 2.0 / (hsize as f64),
            half_width: half_width,
            half_height: half_height,
        }
    }

    pub fn ray_for_pixel(&self, x: u32, y: u32) -> Ray {
        // the offset from the edge of the canvas to the pixel's center
        let xoffset = (x as f64 + 0.5) * self.pixel_size;
        let yoffset = (y as f64 + 0.5) * self.pixel_size;

        // the untransformed coordinates of the pixel in world space
        // (remember that the camera looks toward -z, so +x is to the *left*.)
        let world_x = self.half_width - xoffset;
        let world_y = self.half_height - yoffset;

        // using the camera matrix, transform the canvas point and the origin,
        // and then compute the ray's direction vector.
        // (remember that the canvas is at z=-1)
        let inv_transform = self.transform.inverted();
        let pixel = &inv_transform * &Point3D::new(world_x, world_y, -1.0);
        let origin = &inv_transform * &Point3D::new(0.0, 0.0, 0.0);
        let direction = (&pixel - &origin).normalized();

        Ray::new(origin, direction)
    }

    pub fn render(&self, world: &mut World) -> Canvas {
        let mut image = Canvas::new(self.hsize, self.vsize);

        for y in 0..self.vsize {
            for x in 0..self.hsize {
                let ray = self.ray_for_pixel(x, y);
                let color = world.color_at(&ray);
                image.write_pixel(x, y, color);
            }
        }

        image
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::assert_eqf;
    use crate::color::Color;
    use crate::d3::{Point3D, Vector3D};
    use crate::math::compare_floats;
    use crate::matrix::Matrix4By4;
    use std::f64::consts::PI;

    #[test]
    fn camera_construction() {
        let camera = Camera::new(160, 120, PI / 2.0);

        assert_eq!(camera.hsize, 160);
        assert_eq!(camera.vsize, 120);
        assert_eqf!(camera.field_of_view, PI / 2.0);
        assert_eq!(camera.transform, Matrix4By4::identity());
    }

    #[test]
    fn camera_pixel_size_for_horizontal_canvas() {
        let camera = Camera::new(200, 125, PI / 2.0);

        assert_eqf!(camera.pixel_size, 0.01);
        assert_eqf!(camera.half_width, 1.0);
        assert_eqf!(camera.half_height, 0.625);
    }

    #[test]
    fn camera_pixel_size_for_vertical_canvas() {
        let camera = Camera::new(125, 200, PI / 2.0);

        assert_eqf!(camera.pixel_size, 0.01);
        assert_eqf!(camera.half_width, 0.625);
        assert_eqf!(camera.half_height, 1.0);
    }

    #[test]
    fn constructing_a_ray_through_the_center_of_the_canvas() {
        let camera = Camera::new(201, 101, PI / 2.0);

        let ray = camera.ray_for_pixel(100, 50);

        assert_eq!(ray.origin, Point3D::new(0.0, 0.0, 0.0));
        assert_eq!(ray.direction, Vector3D::new(0.0, 0.0, -1.0));
    }

    #[test]
    fn constructing_a_ray_through_a_corner_of_the_canvas() {
        let camera = Camera::new(201, 101, PI / 2.0);

        let ray = camera.ray_for_pixel(0, 0);

        assert_eq!(ray.origin, Point3D::new(0.0, 0.0, 0.0));
        assert_eq!(ray.direction, Vector3D::new(0.66519, 0.33259, -0.66851));
    }

    #[test]
    fn constructing_a_ray_when_the_camera_is_transformed() {
        let mut camera = Camera::new(201, 101, PI / 2.0);
        camera.transform =
            &Matrix4By4::for_y_rotation(PI / 4.0) * &Matrix4By4::for_translating(0.0, -2.0, 5.0);

        let ray = camera.ray_for_pixel(100, 50);

        assert_eq!(ray.origin, Point3D::new(0.0, 2.0, -5.0));
        let root_two = (2.0_f64).sqrt();
        assert_eq!(
            ray.direction,
            Vector3D::new(root_two / 2.0, 0.0, -root_two / 2.0)
        );
    }

    #[test]
    fn rendering_world_with_camera() {
        let mut world = World::new();
        let mut camera = Camera::new(11, 11, PI / 2.0);
        let from_point = Point3D::new(0.0, 0.0, -5.0);
        let to_point = Point3D::new(0.0, 0.0, 0.0);
        let up_vector = Vector3D::new(0.0, 1.0, 0.0);
        camera.transform = Matrix4By4::view_transform(&from_point, &to_point, &up_vector);

        let image: Canvas = camera.render(&mut world);

        assert_eq!(
            image.pixel_at(5, 5).unwrap().clone(),
            Color::new(0.38066, 0.47583, 0.2855)
        );
    }
}
