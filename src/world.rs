use crate::color::Color;
use crate::d3::Point3D;
use crate::light::PointLight;
use crate::material::Material;
use crate::matrix::Matrix4By4;
use crate::ray::{Computation, Intersection, Ray};
use crate::shape::Shape;

#[derive(Debug)]
pub struct World {
    pub light: PointLight,
    pub objects: Vec<Shape>,
}

impl World {
    pub fn new() -> Self {
        let mut inner_sphere = Shape::sphere();
        inner_sphere.transform = Matrix4By4::for_scaling(0.5, 0.5, 0.5);

        let mut outer_sphere = Shape::sphere();
        let mut material = Material::new();
        material.color = Color::new(0.8, 1.0, 0.6);
        material.diffuse = 0.7;
        material.specular = 0.2;
        outer_sphere.material = material;

        Self {
            light: PointLight::new(Point3D::new(-10.0, 10.0, -10.0), Color::new(1.0, 1.0, 1.0)),
            objects: vec![outer_sphere, inner_sphere],
        }
    }

    pub fn shade_hit(&mut self, computations: &Computation) -> Color {
        let is_shadowed = self.is_shadowed(&computations.over_point);
        computations.object.material.lighting(
            &computations.object,
            &self.light,
            &computations.over_point,
            &computations.eye_vector,
            &computations.normal_vector,
            is_shadowed,
        )
    }

    pub fn color_at(&mut self, ray: &Ray) -> Color {
        let intersections = ray.intersect_world(self);
        let hit = Intersection::hit(&intersections);

        if let None = hit {
            return Color::new(0.0, 0.0, 0.0);
        }

        let computations = hit.unwrap().prepare_computations(ray);
        self.shade_hit(&computations)
    }

    pub fn is_shadowed(&mut self, point: &Point3D) -> bool {
        let point_to_light = &self.light.position - point;
        let distance = point_to_light.abs();
        let shadow_ray = Ray::new(point.clone(), point_to_light.normalized());

        let intersections = shadow_ray.intersect_world(self);

        if let Some(hit) = Intersection::hit(&intersections) {
            return hit.t < distance;
        }

        false
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::d3::Vector3D;

    #[test]
    fn default_world() {
        let light = PointLight::new(Point3D::new(-10.0, 10.0, -10.0), Color::new(1.0, 1.0, 1.0));
        let mut sphere_outer = Shape::sphere();
        sphere_outer.material = Material::new();
        sphere_outer.material.color = Color::new(0.8, 1.0, 0.6);
        sphere_outer.material.diffuse = 0.7;
        sphere_outer.material.specular = 0.2;
        let mut sphere_inner = Shape::sphere();
        sphere_inner.set_transform(Matrix4By4::for_scaling(0.5, 0.5, 0.5));

        let world = World::new();

        assert_eq!(world.light, light);
        assert!(world.objects.contains(&sphere_inner));
        assert!(world.objects.contains(&sphere_outer));
    }

    #[test]
    fn shading_an_intersection() {
        let mut world = World::new();
        let ray = Ray::new(Point3D::new(0.0, 0.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));
        // intersection with outer sphere in world
        let intersection = Intersection::new(4.0, world.objects[0].clone());

        let computations = intersection.prepare_computations(&ray);
        let color = world.shade_hit(&computations);

        assert_eq!(color, Color::new(0.38066, 0.47583, 0.2855));
    }

    #[test]
    fn shading_an_intersection_from_the_inside() {
        let mut world = World::new();
        world.light = PointLight::new(Point3D::new(0.0, 0.25, 0.0), Color::new(1.0, 1.0, 1.0));
        let ray = Ray::new(Point3D::new(0.0, 0.0, 0.0), Vector3D::new(0.0, 0.0, 1.0));
        // intersection with inner sphere in world
        let intersection = Intersection::new(0.5, world.objects[1].clone());

        let computations = intersection.prepare_computations(&ray);
        let color = world.shade_hit(&computations);

        assert_eq!(color, Color::new(0.90498, 0.90498, 0.90498));
    }

    #[test]
    fn color_world_for_ray_that_intersects_nothing() {
        let mut world = World::new();
        let ray = Ray::new(Point3D::new(0.0, 0.0, -5.0), Vector3D::new(0.0, 1.0, 0.0));

        let color = world.color_at(&ray);

        // no color -- black
        assert_eq!(color, Color::new(0.0, 0.0, 0.0));
    }

    #[test]
    fn color_world_for_ray_that_intersects_an_object() {
        let mut world = World::new();
        let ray = Ray::new(Point3D::new(0.0, 0.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));

        let color = world.color_at(&ray);

        assert_eq!(color, Color::new(0.38066, 0.47583, 0.2855));
    }

    #[test]
    fn color_world_for_intersection_behind_ray() {
        let mut world = World::new();
        world.objects[0].material.ambient = 1.0;
        world.objects[1].material.ambient = 1.0;
        let ray = Ray::new(Point3D::new(0.0, 0.0, 0.75), Vector3D::new(0.0, 0.0, -1.0));

        let color = world.color_at(&ray);

        assert_eq!(color, world.objects[1].material.color);
    }

    #[test]
    fn no_shadow_when_nothing_is_collinear_with_point_and_light() {
        let mut world = World::new();
        let point = Point3D::new(0.0, 10.0, 0.0);

        assert!(!world.is_shadowed(&point));
    }

    #[test]
    fn shadow_when_object_is_between_point_and_light() {
        let mut world = World::new();
        let point = Point3D::new(10.0, -10.0, 10.0);

        assert!(world.is_shadowed(&point));
    }

    #[test]
    fn no_shadow_when_object_is_behind_light() {
        let mut world = World::new();
        let point = Point3D::new(-20.0, 20.0, -20.0);

        assert!(!world.is_shadowed(&point));
    }

    #[test]
    fn no_shadow_when_object_is_behind_point() {
        let mut world = World::new();
        let point = Point3D::new(-2.0, 2.0, -2.0);

        assert!(!world.is_shadowed(&point));
    }

    #[test]
    fn consider_shadows_when_shading_hits() {
        let mut world = World::new();
        world.light = PointLight::new(Point3D::new(0.0, 0.0, -10.0), Color::new(1.0, 1.0, 1.0));
        // two objects, with second in the shadow of the first
        let object_a = Shape::sphere();
        world.objects.push(object_a);
        let mut object_b = Shape::sphere();
        object_b.transform = Matrix4By4::for_translating(0.0, 0.0, 10.0);
        world.objects.push(object_b.clone());
        // trace a ray to the in-shadow object -- the second object
        let ray = Ray::new(Point3D::new(0.0, 0.0, 5.0), Vector3D::new(0.0, 0.0, 1.0));
        let intersection = Intersection::new(4.0, object_b);

        let computations = intersection.prepare_computations(&ray);

        assert_eq!(world.shade_hit(&computations), Color::new(0.1, 0.1, 0.1));
    }
}
