use crate::d3::{Point3D, Vector3D};
use crate::math::compare_floats;

#[derive(Debug)]
pub struct RowOf2(f64, f64);

#[derive(Debug)]
pub struct RowOf3(f64, f64, f64);

#[derive(Debug, Clone, Copy)]
pub struct RowOf4(f64, f64, f64, pub f64);

#[derive(Debug)]
pub struct Matrix2By2(RowOf2, RowOf2);

#[derive(Debug)]
pub struct Matrix3By3(RowOf3, RowOf3, RowOf3);

#[derive(Debug, Clone, Copy)]
pub struct Matrix4By4(RowOf4, RowOf4, RowOf4, RowOf4);

impl RowOf2 {
    pub fn new(a: f64, b: f64) -> Self {
        Self(a, b)
    }
}

impl RowOf3 {
    pub fn new(a: f64, b: f64, c: f64) -> Self {
        Self(a, b, c)
    }
}

impl RowOf4 {
    pub fn new(a: f64, b: f64, c: f64, d: f64) -> Self {
        Self(a, b, c, d)
    }
}

impl Matrix2By2 {
    pub fn new(a: RowOf2, b: RowOf2) -> Self {
        Self(a, b)
    }

    pub fn identity() -> Self {
        Self::new(RowOf2::new(1.0, 0.0), RowOf2::new(0.0, 1.0))
    }

    fn determinant(&self) -> f64 {
        (self[0][0] * self[1][1]) - (self[0][1] * self[1][0])
    }
}

impl Matrix3By3 {
    pub fn new(a: RowOf3, b: RowOf3, c: RowOf3) -> Self {
        Self(a, b, c)
    }

    pub fn identity() -> Self {
        Matrix3By3::new(
            RowOf3::new(1.0, 0.0, 0.0),
            RowOf3::new(0.0, 1.0, 0.0),
            RowOf3::new(0.0, 0.0, 1.0),
        )
    }

    pub fn determinant(&self) -> f64 {
        let mut res = 0.0;
        for j in 0..3 {
            res += self[1][j] * self.cofactor(1, j);
        }

        res
    }

    fn submatrix(&self, pivot_row: usize, pivot_col: usize) -> Matrix2By2 {
        let mut res = Matrix2By2::identity();

        for i in 0..2 {
            let mut row_idx = i;
            if i >= pivot_row {
                row_idx += 1;
            }
            for j in 0..2 {
                let mut col_idx = j;
                if j >= pivot_col {
                    col_idx += 1;
                }

                res[i][j] = self[row_idx][col_idx];
            }
        }

        res
    }

    fn minor(&self, pivot_row: usize, pivot_col: usize) -> f64 {
        self.submatrix(pivot_row, pivot_col).determinant()
    }

    fn cofactor(&self, row: usize, col: usize) -> f64 {
        self.minor(row, col)
            * match (row + col) % 2 {
                0 => 1.0,
                _ => -1.0,
            }
    }
}

impl Matrix4By4 {
    pub fn new(a: RowOf4, b: RowOf4, c: RowOf4, d: RowOf4) -> Self {
        Self(a, b, c, d)
    }

    pub fn identity() -> Self {
        Self::new(
            RowOf4::new(1.0, 0.0, 0.0, 0.0),
            RowOf4::new(0.0, 1.0, 0.0, 0.0),
            RowOf4::new(0.0, 0.0, 1.0, 0.0),
            RowOf4::new(0.0, 0.0, 0.0, 1.0),
        )
    }

    pub fn for_translating(x: f64, y: f64, z: f64) -> Self {
        let mut res = Self::identity();

        res[0][3] = x;
        res[1][3] = y;
        res[2][3] = z;

        res
    }

    pub fn for_scaling(x: f64, y: f64, z: f64) -> Self {
        let mut res = Self::identity();

        res[0][0] = x;
        res[1][1] = y;
        res[2][2] = z;

        res
    }

    pub fn for_x_rotation(radians: f64) -> Self {
        let mut res = Self::identity();

        res[1][1] = radians.cos();
        res[2][1] = radians.sin();
        res[1][2] = -res[2][1];
        res[2][2] = -res[1][1];

        res
    }

    pub fn for_y_rotation(radians: f64) -> Self {
        let mut res = Self::identity();

        res[0][0] = radians.cos();
        res[0][2] = radians.sin();
        res[2][0] = -res[0][2];
        res[2][2] = res[0][0];

        res
    }

    pub fn for_z_rotation(radians: f64) -> Self {
        let mut res = Self::identity();

        res[0][0] = radians.cos();
        res[1][0] = radians.sin();
        res[0][1] = -res[1][0];
        res[1][1] = res[0][0];

        res
    }

    pub fn for_shearing(
        x_wrt_y: f64,
        x_wrt_z: f64,
        y_wrt_x: f64,
        y_wrt_z: f64,
        z_wrt_x: f64,
        z_wrt_y: f64,
    ) -> Self {
        let mut res = Self::identity();

        res[0][1] = x_wrt_y;
        res[0][2] = x_wrt_z;
        res[1][0] = y_wrt_x;
        res[1][2] = y_wrt_z;
        res[2][0] = z_wrt_x;
        res[2][1] = z_wrt_y;
        res
    }

    pub fn view_transform(from_point: &Point3D, to_point: &Point3D, up_vector: &Vector3D) -> Self {
        let mut orientation = Self::identity();

        let forward_vector = (to_point - from_point).normalized();
        let left_vector = forward_vector.cross(&up_vector.normalized());
        let true_up = left_vector.cross(&forward_vector);

        orientation[0][0] = left_vector.x;
        orientation[0][1] = left_vector.y;
        orientation[0][2] = left_vector.z;
        orientation[1][0] = true_up.x;
        orientation[1][1] = true_up.y;
        orientation[1][2] = true_up.z;
        orientation[2][0] = -(forward_vector.x);
        orientation[2][1] = -(forward_vector.y);
        orientation[2][2] = -(forward_vector.z);

        &orientation
            * &Matrix4By4::for_translating(-(from_point.x), -(from_point.y), -(from_point.z))
    }

    pub fn transposed(&self) -> Self {
        Self::new(
            self.column_at(0),
            self.column_at(1),
            self.column_at(2),
            self.column_at(3),
        )
    }

    pub fn determinant(&self) -> f64 {
        let mut res = 0.0;
        for j in 0..4 {
            res += self[1][j] * self.cofactor(1, j);
        }

        res
    }

    pub fn is_invertible(&self) -> bool {
        !compare_floats(self.determinant(), 0.0)
    }

    pub fn inverted(&self) -> Self {
        let det = self.determinant();
        let mut res = Self::identity();

        for i in 0..4 {
            for j in 0..4 {
                // reverse the order of i and j so there's no
                // need to transpose at the end
                res[j][i] = self.cofactor(i, j) / det;
            }
        }

        res
    }

    fn submatrix(&self, pivot_row: usize, pivot_col: usize) -> Matrix3By3 {
        let mut res = Matrix3By3::identity();

        for i in 0..3 {
            let mut row_idx = i;
            if i >= pivot_row {
                row_idx += 1;
            }
            for j in 0..3 {
                let mut col_idx = j;
                if j >= pivot_col {
                    col_idx += 1;
                }

                res[i][j] = self[row_idx][col_idx];
            }
        }

        res
    }

    fn minor(&self, pivot_row: usize, pivot_col: usize) -> f64 {
        self.submatrix(pivot_row, pivot_col).determinant()
    }

    fn cofactor(&self, row: usize, col: usize) -> f64 {
        self.minor(row, col)
            * match (row + col) % 2 {
                0 => 1.0,
                _ => -1.0,
            }
    }

    fn column_at(&self, index: usize) -> RowOf4 {
        RowOf4(self.0[index], self.1[index], self.2[index], self.3[index])
    }
}

impl std::cmp::PartialEq for RowOf2 {
    fn eq(&self, other: &Self) -> bool {
        compare_floats(self.0, other.0) && compare_floats(self.1, other.1)
    }
}

impl std::ops::Index<usize> for RowOf2 {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.0,
            1 => &self.1,
            n => panic!("Index `{}` out of bounds for RowOf2 type.", n),
        }
    }
}

impl std::ops::IndexMut<usize> for RowOf2 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            n => panic!("Index `{}` out of bounds for RowOf2 type.", n),
        }
    }
}

impl std::ops::Mul for &RowOf2 {
    type Output = f64;

    fn mul(self, other: Self) -> Self::Output {
        (self.0 * other.0) + (self.1 * other.1)
    }
}

impl std::cmp::PartialEq for RowOf3 {
    fn eq(&self, other: &Self) -> bool {
        compare_floats(self.0, other.0)
            && compare_floats(self.1, other.1)
            && compare_floats(self.2, other.2)
    }
}

impl std::ops::Index<usize> for RowOf3 {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.0,
            1 => &self.1,
            2 => &self.2,
            n => panic!("Index `{}` out of bounds for RowOf3 type.", n),
        }
    }
}

impl std::ops::IndexMut<usize> for RowOf3 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            2 => &mut self.2,
            n => panic!("Index `{}` out of bounds for RowOf3 type.", n),
        }
    }
}

impl std::ops::Mul for &RowOf3 {
    type Output = f64;

    fn mul(self, other: Self) -> Self::Output {
        (self.0 * other.0) + (self.1 * other.1) + (self.2 * other.2)
    }
}

impl std::cmp::PartialEq for RowOf4 {
    fn eq(&self, other: &Self) -> bool {
        compare_floats(self.0, other.0)
            && compare_floats(self.1, other.1)
            && compare_floats(self.2, other.2)
            && compare_floats(self.3, other.3)
    }
}

impl std::ops::Index<usize> for RowOf4 {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.0,
            1 => &self.1,
            2 => &self.2,
            3 => &self.3,
            n => panic!("Index `{}` out of bounds for RowOf4 type.", n),
        }
    }
}

impl std::ops::IndexMut<usize> for RowOf4 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            2 => &mut self.2,
            3 => &mut self.3,
            n => panic!("Index `{}` out of bounds for RowOf4 type.", n),
        }
    }
}

impl std::ops::Mul for &RowOf4 {
    type Output = f64;

    fn mul(self, other: Self) -> Self::Output {
        (self.0 * other.0) + (self.1 * other.1) + (self.2 * other.2) + (self.3 * other.3)
    }
}

impl std::cmp::PartialEq for Matrix2By2 {
    fn eq(&self, other: &Self) -> bool {
        (self.0 == other.0) && (self.1 == other.1)
    }
}

impl std::ops::Index<usize> for Matrix2By2 {
    type Output = RowOf2;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.0,
            1 => &self.1,
            n => panic!("Index `{}` out of bounds for Matix2By2 type.", n),
        }
    }
}

impl std::ops::IndexMut<usize> for Matrix2By2 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            n => panic!("Index `{}` out of bounds for Matix2By2 type.", n),
        }
    }
}

impl std::cmp::PartialEq for Matrix3By3 {
    fn eq(&self, other: &Self) -> bool {
        (self.0 == other.0) && (self.1 == other.1) && (self.2 == other.2)
    }
}

impl std::ops::Index<usize> for Matrix3By3 {
    type Output = RowOf3;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.0,
            1 => &self.1,
            2 => &self.2,
            n => panic!("Index `{}` out of bounds for Matrix3By3 type.", n),
        }
    }
}

impl std::ops::IndexMut<usize> for Matrix3By3 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            2 => &mut self.2,
            n => panic!("Index `{}` out of bounds for Matrix3By3 type.", n),
        }
    }
}

impl std::cmp::PartialEq for Matrix4By4 {
    fn eq(&self, other: &Self) -> bool {
        (self.0 == other.0) && (self.1 == other.1) && (self.2 == other.2) && (self.3 == other.3)
    }
}

impl std::ops::Index<usize> for Matrix4By4 {
    type Output = RowOf4;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.0,
            1 => &self.1,
            2 => &self.2,
            3 => &self.3,
            n => panic!("Index `{}` out of bounds for Matrix4By4 type.", n),
        }
    }
}

impl std::ops::IndexMut<usize> for Matrix4By4 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            2 => &mut self.2,
            3 => &mut self.3,
            n => panic!("Index `{}` out of bounds for Matrix3By3 type.", n),
        }
    }
}

impl std::ops::Mul for &Matrix4By4 {
    type Output = Matrix4By4;

    fn mul(self, other: Self) -> Self::Output {
        let mut res = Self::Output::identity();

        for j in 0..4 {
            let multiplying_column = other.column_at(j);
            for i in 0..4 {
                res[i][j] = &self[i] * &multiplying_column;
            }
        }

        res
    }
}

impl std::ops::Mul<&RowOf4> for &Matrix4By4 {
    type Output = RowOf4;

    fn mul(self, row: &RowOf4) -> Self::Output {
        RowOf4::new(&self.0 * row, &self.1 * row, &self.2 * row, &self.3 * row)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::assert_eqf;
    use std::f64::consts::PI;
    use std::panic::catch_unwind;

    #[test]
    fn row_of_2_equality() {
        let row = RowOf2::new(0.0, 0.1);

        assert_eq!(row, RowOf2::new(0.0, 0.1));
        assert_ne!(row, RowOf2::new(0.0, 0.0));
        assert_ne!(row, RowOf2::new(0.1, 0.1));
    }

    #[test]
    fn row_of_2_indexing() {
        let row = RowOf2::new(0.0, 0.1);

        assert_eqf!(row[0], 0.0);
        assert_eqf!(row[1], 0.1);
        let res = catch_unwind(|| &row[2]);
        assert!(res.is_err());
    }

    #[test]
    fn row_of_2_indexing_for_mut() {
        let mut row = RowOf2::new(0.0, 0.1);

        row[0] = 1.0;
        row[1] = -1.0;

        assert_eq!(row, RowOf2::new(1.0, -1.0));
        let res = catch_unwind(move || row[2] = 0.0);
        assert!(res.is_err());
    }

    #[test]
    fn row_of_2_multiplication() {
        let row_a = RowOf2::new(1.0, 2.0);
        let row_b = RowOf2::new(3.0, 4.0);

        assert_eqf!(&row_a * &row_b, 11.0);
    }

    #[test]
    fn row_of_3_equality() {
        let row = RowOf3::new(0.0, 0.1, 0.2);

        assert_eq!(row, RowOf3::new(0.0, 0.1, 0.2));
        assert_ne!(row, RowOf3::new(0.0, 0.1, 0.3));
        assert_ne!(row, RowOf3::new(0.0, 0.2, 0.2));
        assert_ne!(row, RowOf3::new(0.1, 0.1, 0.2));
    }

    #[test]
    fn row_of_3_indexing() {
        let row = RowOf3::new(0.0, 0.1, 0.2);

        assert_eqf!(row[0], 0.0);
        assert_eqf!(row[1], 0.1);
        assert_eqf!(row[2], 0.2);
        let res = catch_unwind(|| &row[3]);
        assert!(res.is_err());
    }

    #[test]
    fn row_of_3_indexing_for_mut() {
        let mut row = RowOf3::new(0.0, 0.1, 0.2);

        row[0] = 1.0;
        row[1] = -1.0;
        row[2] = 1.0;

        assert_eq!(row, RowOf3::new(1.0, -1.0, 1.0));
        let res = catch_unwind(move || row[3] = 0.0);
        assert!(res.is_err());
    }

    #[test]
    fn row_of_3_multiplication() {
        let row_a = RowOf3::new(1.0, 2.0, 3.0);
        let row_b = RowOf3::new(3.0, 4.0, 5.0);

        assert_eqf!(&row_a * &row_b, 26.0);
    }

    #[test]
    fn row_of_4_equality() {
        let row = RowOf4::new(0.0, 0.1, 0.2, 0.3);

        assert_eq!(row, RowOf4::new(0.0, 0.1, 0.2, 0.3));
        assert_ne!(row, RowOf4::new(0.0, 0.1, 0.2, 0.0));
        assert_ne!(row, RowOf4::new(0.0, 0.1, 0.0, 0.3));
        assert_ne!(row, RowOf4::new(0.0, 0.0, 0.2, 0.3));
        assert_ne!(row, RowOf4::new(0.00001, 0.1, 0.2, 0.3));
    }

    #[test]
    fn row_of_4_indexing() {
        let row = RowOf4::new(0.0, 0.1, 0.2, 0.3);

        assert_eqf!(row[0], 0.0);
        assert_eqf!(row[1], 0.1);
        assert_eqf!(row[2], 0.2);
        assert_eqf!(row[3], 0.3);
        let res = catch_unwind(|| &row[4]);
        assert!(res.is_err());
    }

    #[test]
    fn row_of_4_indexing_for_mut() {
        let mut row = RowOf4::new(0.0, 0.1, 0.2, 0.3);

        row[0] = 1.0;
        row[1] = -1.0;
        row[2] = 1.0;
        row[3] = -1.0;

        assert_eq!(row, RowOf4::new(1.0, -1.0, 1.0, -1.0));
        let res = catch_unwind(move || row[4] = 0.0);
        assert!(res.is_err());
    }

    #[test]
    fn row_of_4_multiplication() {
        let row_a = RowOf4::new(1.0, 2.0, 3.0, 4.0);
        let row_b = RowOf4::new(3.0, 4.0, 5.0, 6.0);

        assert_eqf!(&row_a * &row_b, 50.0);
    }

    #[test]
    fn matrix_2x2_equality() {
        let matrix = Matrix2By2::new(RowOf2::new(1.0, 2.0), RowOf2::new(3.0, 4.0));

        assert_eq!(
            matrix,
            Matrix2By2::new(RowOf2::new(1.0, 2.0), RowOf2::new(3.0, 4.0))
        );
        assert_ne!(
            matrix,
            Matrix2By2::new(RowOf2::new(1.0, 2.0), RowOf2::new(2.0, 4.0))
        );
    }

    #[test]
    fn matrix_2x2_identity_value() {
        let matrix = Matrix2By2::identity();

        assert_eq!(
            matrix,
            Matrix2By2::new(RowOf2::new(1.0, 0.0), RowOf2::new(0.0, 1.0),)
        );
    }

    #[test]
    fn matrix_2x2_indexing() {
        let matrix = Matrix2By2::new(RowOf2::new(1.0, 2.0), RowOf2::new(3.0, 4.0));

        assert_eq!(matrix[0], RowOf2::new(1.0, 2.0));
        assert_eq!(matrix[1], RowOf2::new(3.0, 4.0));
        let res = catch_unwind(|| &matrix[2]);
        assert!(res.is_err());
    }

    #[test]
    fn matrix_2x2_indexing_for_mut() {
        let mut matrix = Matrix2By2::new(RowOf2::new(1.0, 2.0), RowOf2::new(3.0, 4.0));

        matrix[0] = RowOf2::new(5.0, 0.5);
        matrix[1] = RowOf2::new(0.5, 5.0);

        assert_eq!(
            matrix,
            Matrix2By2::new(RowOf2::new(5.0, 0.5), RowOf2::new(0.5, 5.0))
        );
        let res = catch_unwind(move || matrix[2] = RowOf2::new(0.5, 5.0));
        assert!(res.is_err());
    }

    #[test]
    fn matrix_2x2_determinant() {
        assert_eqf!(
            Matrix2By2::new(RowOf2::new(1.0, 2.0), RowOf2::new(3.0, 4.0)).determinant(),
            -2.0
        );
        assert_eqf!(
            Matrix2By2::new(RowOf2::new(1.0, 5.0), RowOf2::new(-3.0, 2.0)).determinant(),
            17.0
        );
    }

    #[test]
    fn matrix_3x3_equality() {
        let matrix = Matrix3By3::new(
            RowOf3::new(1.0, 2.0, 3.0),
            RowOf3::new(4.0, 5.0, 6.0),
            RowOf3::new(7.0, 8.0, 9.0),
        );

        assert_eq!(
            matrix,
            Matrix3By3::new(
                RowOf3::new(1.0, 2.0, 3.0),
                RowOf3::new(4.0, 5.0, 6.0),
                RowOf3::new(7.0, 8.0, 9.0)
            )
        );
        assert_ne!(
            matrix,
            Matrix3By3::new(
                RowOf3::new(0.0, 2.0, 3.0),
                RowOf3::new(4.0, 5.0, 6.0),
                RowOf3::new(7.0, 8.0, 9.0)
            )
        );
    }

    #[test]
    fn matrix_3x3_identity_value() {
        let matrix = Matrix3By3::identity();

        assert_eq!(
            matrix,
            Matrix3By3::new(
                RowOf3::new(1.0, 0.0, 0.0),
                RowOf3::new(0.0, 1.0, 0.0),
                RowOf3::new(0.0, 0.0, 1.0),
            )
        );
    }

    #[test]
    fn matrix_3x3_indexing() {
        let matrix = Matrix3By3::new(
            RowOf3::new(1.0, 2.0, 3.0),
            RowOf3::new(4.0, 5.0, 6.0),
            RowOf3::new(7.0, 8.0, 9.0),
        );

        assert_eq!(matrix[0], RowOf3::new(1.0, 2.0, 3.0));
        assert_eq!(matrix[1], RowOf3::new(4.0, 5.0, 6.0));
        assert_eq!(matrix[2], RowOf3::new(7.0, 8.0, 9.0));
        let res = catch_unwind(|| &matrix[3]);
        assert!(res.is_err());
    }

    #[test]
    fn matrix_3x3_indexing_for_mut() {
        let mut matrix = Matrix3By3::new(
            RowOf3::new(1.0, 2.0, 3.0),
            RowOf3::new(4.0, 5.0, 6.0),
            RowOf3::new(7.0, 8.0, 9.0),
        );

        matrix[0] = RowOf3::new(9.0, 8.0, 7.0);
        matrix[1] = RowOf3::new(6.0, 5.0, 4.0);
        matrix[2] = RowOf3::new(1.0, 2.0, 3.0);

        assert_eq!(
            matrix,
            Matrix3By3::new(
                RowOf3::new(9.0, 8.0, 7.0),
                RowOf3::new(6.0, 5.0, 4.0),
                RowOf3::new(1.0, 2.0, 3.0)
            )
        );
        let res = catch_unwind(move || matrix[3] = RowOf3::new(1.0, 2.0, 3.0));
        assert!(res.is_err());
    }

    #[test]
    fn matrix_3x3_submatrices() {
        let matrix = Matrix3By3::new(
            RowOf3::new(1.0, 5.0, 0.0),
            RowOf3::new(-3.0, 2.0, 7.0),
            RowOf3::new(0.0, 6.0, -3.0),
        );

        assert_eq!(
            matrix.submatrix(0, 2),
            Matrix2By2::new(RowOf2::new(-3.0, 2.0), RowOf2::new(0.0, 6.0),)
        );
        assert_eq!(
            matrix.submatrix(1, 1),
            Matrix2By2::new(RowOf2::new(1.0, 0.0), RowOf2::new(0.0, -3.0),)
        );
    }

    #[test]
    fn matrix_3x3_minors() {
        let matrix = Matrix3By3::new(
            RowOf3::new(3.0, 5.0, 0.0),
            RowOf3::new(2.0, -1.0, -7.0),
            RowOf3::new(6.0, -1.0, 5.0),
        );

        assert_eqf!(matrix.minor(0, 0), -12.0);
        assert_eqf!(matrix.minor(1, 0), 25.0);
    }

    #[test]
    fn matrix_3x3_cofactors() {
        let matrix = Matrix3By3::new(
            RowOf3::new(3.0, 5.0, 0.0),
            RowOf3::new(2.0, -1.0, -7.0),
            RowOf3::new(6.0, -1.0, 5.0),
        );

        assert_eqf!(matrix.cofactor(0, 0), -12.0);
        assert_eqf!(matrix.cofactor(1, 0), -25.0);
    }

    #[test]
    fn matrix_3x3_determinant() {
        let matrix = Matrix3By3::new(
            RowOf3::new(1.0, 2.0, 6.0),
            RowOf3::new(-5.0, 8.0, -4.0),
            RowOf3::new(2.0, 6.0, 4.0),
        );

        assert_eqf!(matrix.determinant(), -196.0);
    }

    #[test]
    fn matrix_4x4_equality() {
        let matrix = Matrix4By4::new(
            RowOf4::new(1.0, 2.0, 3.0, 4.0),
            RowOf4::new(5.0, 6.0, 7.0, 8.0),
            RowOf4::new(9.0, 8.0, 7.0, 6.0),
            RowOf4::new(5.0, 4.0, 3.0, 2.0),
        );

        assert_eq!(
            matrix,
            Matrix4By4::new(
                RowOf4::new(1.0, 2.0, 3.0, 4.0),
                RowOf4::new(5.0, 6.0, 7.0, 8.0),
                RowOf4::new(9.0, 8.0, 7.0, 6.0),
                RowOf4::new(5.0, 4.0, 3.0, 2.0),
            )
        );
        assert_ne!(
            matrix,
            Matrix4By4::new(
                RowOf4::new(0.0, 2.0, 3.0, 4.0),
                RowOf4::new(5.0, 6.0, 7.0, 8.0),
                RowOf4::new(9.0, 8.0, 7.0, 6.0),
                RowOf4::new(5.0, 4.0, 3.0, 2.0),
            )
        );
    }

    #[test]
    fn matrix_4x4_identity_value() {
        let matrix = Matrix4By4::identity();

        assert_eq!(
            matrix,
            Matrix4By4::new(
                RowOf4::new(1.0, 0.0, 0.0, 0.0),
                RowOf4::new(0.0, 1.0, 0.0, 0.0),
                RowOf4::new(0.0, 0.0, 1.0, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        )
    }

    #[test]
    fn matrix_4x4_indexing() {
        let matrix = Matrix4By4::new(
            RowOf4::new(1.0, 2.0, 3.0, 4.0),
            RowOf4::new(5.0, 6.0, 7.0, 8.0),
            RowOf4::new(9.0, 8.0, 7.0, 6.0),
            RowOf4::new(5.0, 4.0, 3.0, 2.0),
        );

        assert_eq!(matrix[0], RowOf4::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(matrix[1], RowOf4::new(5.0, 6.0, 7.0, 8.0));
        assert_eq!(matrix[2], RowOf4::new(9.0, 8.0, 7.0, 6.0));
        assert_eq!(matrix[3], RowOf4::new(5.0, 4.0, 3.0, 2.0));
        let res = catch_unwind(|| &matrix[4]);
        assert!(res.is_err());
    }

    #[test]
    fn matrix_4x4_indexing_for_mut() {
        let mut matrix = Matrix4By4::new(
            RowOf4::new(1.0, 2.0, 3.0, 4.0),
            RowOf4::new(5.0, 6.0, 7.0, 8.0),
            RowOf4::new(9.0, 8.0, 7.0, 6.0),
            RowOf4::new(5.0, 4.0, 3.0, 2.0),
        );

        matrix[0] = RowOf4::new(0.0, 0.0, 0.0, 0.0);
        matrix[1] = RowOf4::new(1.0, 1.0, 1.0, 1.0);
        matrix[2] = RowOf4::new(2.0, 2.0, 2.0, 2.0);
        matrix[3] = RowOf4::new(3.0, 3.0, 3.0, 3.0);

        assert_eq!(
            matrix,
            Matrix4By4::new(
                RowOf4::new(0.0, 0.0, 0.0, 0.0),
                RowOf4::new(1.0, 1.0, 1.0, 1.0),
                RowOf4::new(2.0, 2.0, 2.0, 2.0),
                RowOf4::new(3.0, 3.0, 3.0, 3.0),
            )
        );
        let res = catch_unwind(move || matrix[4] = RowOf4::new(1.0, 2.0, 3.0, 4.0));
        assert!(res.is_err());
    }

    #[test]
    fn matrix_4x4_submatrices() {
        let matrix = Matrix4By4::new(
            RowOf4::new(-6.0, 1.0, 1.0, 6.0),
            RowOf4::new(-8.0, 5.0, 8.0, 6.0),
            RowOf4::new(-1.0, 0.0, 8.0, 2.0),
            RowOf4::new(-7.0, 1.0, -1.0, 1.0),
        );

        assert_eq!(
            matrix.submatrix(0, 2),
            Matrix3By3::new(
                RowOf3::new(-8.0, 5.0, 6.0),
                RowOf3::new(-1.0, 0.0, 2.0),
                RowOf3::new(-7.0, 1.0, 1.0),
            )
        );

        assert_eq!(
            matrix.submatrix(1, 1),
            Matrix3By3::new(
                RowOf3::new(-6.0, 1.0, 6.0),
                RowOf3::new(-1.0, 8.0, 2.0),
                RowOf3::new(-7.0, -1.0, 1.0),
            )
        );

        assert_eq!(
            matrix.submatrix(2, 1),
            Matrix3By3::new(
                RowOf3::new(-6.0, 1.0, 6.0),
                RowOf3::new(-8.0, 8.0, 6.0),
                RowOf3::new(-7.0, -1.0, 1.0),
            )
        );
    }

    #[test]
    fn matrix_4x4_column_at() {
        // !NOTE: This test is for a private method!
        let matrix = Matrix4By4::new(
            RowOf4::new(0.0, 0.1, 0.2, 0.3),
            RowOf4::new(1.0, 1.1, 1.2, 1.3),
            RowOf4::new(2.0, 2.1, 2.2, 2.3),
            RowOf4::new(3.0, 3.1, 3.2, 3.3),
        );

        assert_eq!(matrix.column_at(0), RowOf4::new(0.0, 1.0, 2.0, 3.0));
        assert_eq!(matrix.column_at(1), RowOf4::new(0.1, 1.1, 2.1, 3.1));
        assert_eq!(matrix.column_at(2), RowOf4::new(0.2, 1.2, 2.2, 3.2));
        assert_eq!(matrix.column_at(3), RowOf4::new(0.3, 1.3, 2.3, 3.3));
    }

    #[test]
    fn matrix_4x4_multiplication() {
        let matrix_a = Matrix4By4::new(
            RowOf4::new(1.0, 2.0, 3.0, 4.0),
            RowOf4::new(5.0, 6.0, 7.0, 8.0),
            RowOf4::new(9.0, 8.0, 7.0, 6.0),
            RowOf4::new(5.0, 4.0, 3.0, 2.0),
        );
        let matrix_b = Matrix4By4::new(
            RowOf4::new(-2.0, 1.0, 2.0, 3.0),
            RowOf4::new(3.0, 2.0, 1.0, -1.0),
            RowOf4::new(4.0, 3.0, 6.0, 5.0),
            RowOf4::new(1.0, 2.0, 7.0, 8.0),
        );

        assert_eq!(
            &matrix_a * &matrix_b,
            Matrix4By4::new(
                RowOf4::new(20.0, 22.0, 50.0, 48.0),
                RowOf4::new(44.0, 54.0, 114.0, 108.0),
                RowOf4::new(40.0, 58.0, 110.0, 102.0),
                RowOf4::new(16.0, 26.0, 46.0, 42.0),
            )
        );
    }

    #[test]
    fn matrix_4x4_minors() {
        let matrix = Matrix4By4::new(
            RowOf4::new(-2.0, -8.0, 3.0, 5.0),
            RowOf4::new(-3.0, 1.0, 7.0, 3.0),
            RowOf4::new(1.0, 2.0, -9.0, 6.0),
            RowOf4::new(-6.0, 7.0, 7.0, -9.0),
        );

        assert_eq!(matrix.minor(0, 0), 690.0);
        assert_eq!(matrix.minor(0, 1), -447.0);
        assert_eq!(matrix.minor(0, 2), 210.0);
        assert_eq!(matrix.minor(0, 3), -51.0);
    }

    #[test]
    fn matrix_4x4_cofactors() {
        let matrix = Matrix4By4::new(
            RowOf4::new(-2.0, -8.0, 3.0, 5.0),
            RowOf4::new(-3.0, 1.0, 7.0, 3.0),
            RowOf4::new(1.0, 2.0, -9.0, 6.0),
            RowOf4::new(-6.0, 7.0, 7.0, -9.0),
        );

        assert_eq!(matrix.cofactor(0, 0), 690.0);
        assert_eq!(matrix.cofactor(0, 1), 447.0);
        assert_eq!(matrix.cofactor(0, 2), 210.0);
        assert_eq!(matrix.cofactor(0, 3), 51.0);
    }

    #[test]
    fn matrix_4x4_determinant() {
        let matrix = Matrix4By4::new(
            RowOf4::new(-2.0, -8.0, 3.0, 5.0),
            RowOf4::new(-3.0, 1.0, 7.0, 3.0),
            RowOf4::new(1.0, 2.0, -9.0, 6.0),
            RowOf4::new(-6.0, 7.0, 7.0, -9.0),
        );

        assert_eq!(matrix.determinant(), -4071.0);
    }

    #[test]
    fn matrix_4x4_multiplication_by_row_of_4() {
        let matrix = Matrix4By4::new(
            RowOf4::new(1.0, 2.0, 3.0, 4.0),
            RowOf4::new(2.0, 4.0, 4.0, 2.0),
            RowOf4::new(8.0, 6.0, 4.0, 1.0),
            RowOf4::new(0.0, 0.0, 0.0, 1.0),
        );
        let row = RowOf4::new(1.0, 2.0, 3.0, 1.0);

        assert_eq!(&matrix * &row, RowOf4::new(18.0, 24.0, 33.0, 1.0));
        println!("{:?}", matrix);
    }

    #[test]
    fn matrix_4x4_transposition() {
        let matrix = Matrix4By4::new(
            RowOf4::new(0.0, 0.1, 0.2, 0.3),
            RowOf4::new(1.0, 1.1, 1.2, 1.3),
            RowOf4::new(2.0, 2.1, 2.2, 2.3),
            RowOf4::new(3.0, 3.1, 3.2, 3.3),
        );

        assert_eq!(
            matrix.transposed(),
            Matrix4By4::new(
                RowOf4::new(0.0, 1.0, 2.0, 3.0),
                RowOf4::new(0.1, 1.1, 2.1, 3.1),
                RowOf4::new(0.2, 1.2, 2.2, 3.2),
                RowOf4::new(0.3, 1.3, 2.3, 3.3),
            )
        );
        assert_eq!(Matrix4By4::identity().transposed(), Matrix4By4::identity());
    }

    #[test]
    fn matrix_4x4_invertibility_test() {
        let invertible = Matrix4By4::new(
            RowOf4::new(6.0, 4.0, 4.0, 4.0),
            RowOf4::new(5.0, 5.0, 7.0, 6.0),
            RowOf4::new(4.0, -9.0, 3.0, -7.0),
            RowOf4::new(9.0, 1.0, 7.0, -6.0),
        );
        let noninvertible = Matrix4By4::new(
            RowOf4::new(-4.0, 2.0, 2.0, -3.0),
            RowOf4::new(9.0, 6.0, 2.0, 6.0),
            RowOf4::new(0.0, -5.0, 1.0, -5.0),
            RowOf4::new(0.0, 0.0, 0.0, 0.0),
        );

        assert_eqf!(invertible.determinant(), -2120.0);
        assert_eqf!(noninvertible.determinant(), 0.0);
        assert!(invertible.is_invertible());
        assert!(!noninvertible.is_invertible());
    }

    #[test]
    fn matrix_4x4_inversion() {
        let matrix_a = Matrix4By4::new(
            RowOf4::new(-5.0, 2.0, 6.0, -8.0),
            RowOf4::new(1.0, -5.0, 1.0, 8.0),
            RowOf4::new(7.0, 7.0, -6.0, -7.0),
            RowOf4::new(1.0, -3.0, 7.0, 4.0),
        );
        let matrix_b = Matrix4By4::new(
            RowOf4::new(8.0, -5.0, 9.0, 2.0),
            RowOf4::new(7.0, 5.0, 6.0, 1.0),
            RowOf4::new(-6.0, 0.0, 9.0, 6.0),
            RowOf4::new(-3.0, 0.0, -9.0, -4.0),
        );

        assert_eqf!(matrix_a.determinant(), 532.0);
        assert_eq!(
            matrix_a.inverted(),
            Matrix4By4::new(
                RowOf4::new(0.21805, 0.45113, 0.2406, -0.04511),
                RowOf4::new(-0.80827, -1.45677, -0.44361, 0.52068),
                RowOf4::new(-0.07895, -0.22368, -0.05263, 0.19737),
                RowOf4::new(-0.52256, -0.81391, -0.30075, 0.30639),
            )
        );
        assert_eq!(
            matrix_b.inverted(),
            Matrix4By4::new(
                RowOf4::new(-0.15385, -0.15385, -0.28205, -0.53846),
                RowOf4::new(-0.07692, 0.12308, 0.02564, 0.03077),
                RowOf4::new(0.35897, 0.35897, 0.43590, 0.92308),
                RowOf4::new(-0.69231, -0.69231, -0.76923, -1.92308),
            )
        );
    }

    #[test]
    fn matrix_4x4_inverse_relationship() {
        let matrix_a = Matrix4By4::new(
            RowOf4::new(-6.0, 1.0, 1.0, 6.0),
            RowOf4::new(-8.0, 5.0, 8.0, 6.0),
            RowOf4::new(-1.0, 0.0, 8.0, 2.0),
            RowOf4::new(-7.0, 1.0, -1.0, 1.0),
        );
        let matrix_b = Matrix4By4::new(
            RowOf4::new(1.0, 4.0, 4.0, 4.0),
            RowOf4::new(5.0, 5.0, 7.0, 6.0),
            RowOf4::new(4.0, -9.0, 3.0, -7.0),
            RowOf4::new(9.0, 1.0, 7.0, -6.0),
        );

        assert_eq!(&(&matrix_a * &matrix_b) * &matrix_b.inverted(), matrix_a);
        assert_eq!(&(&matrix_b * &matrix_a) * &matrix_a.inverted(), matrix_b);
    }

    #[test]
    fn translation_matrix() {
        let t1 = Matrix4By4::for_translating(-2.0, -3.0, -4.0);
        let t2 = Matrix4By4::for_translating(1.0, 1.0, -9.0);

        assert_eq!(
            t1,
            Matrix4By4::new(
                RowOf4::new(1.0, 0.0, 0.0, -2.0),
                RowOf4::new(0.0, 1.0, 0.0, -3.0),
                RowOf4::new(0.0, 0.0, 1.0, -4.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
        assert_eq!(
            t2,
            Matrix4By4::new(
                RowOf4::new(1.0, 0.0, 0.0, 1.0),
                RowOf4::new(0.0, 1.0, 0.0, 1.0),
                RowOf4::new(0.0, 0.0, 1.0, -9.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
    }

    #[test]
    fn scaling_matrix() {
        let s1 = Matrix4By4::for_scaling(-2.0, -3.0, -4.0);
        let s2 = Matrix4By4::for_scaling(1.0, 1.0, -9.0);

        assert_eq!(
            s1,
            Matrix4By4::new(
                RowOf4::new(-2.0, 0.0, 0.0, 0.0),
                RowOf4::new(0.0, -3.0, 0.0, 0.0),
                RowOf4::new(0.0, 0.0, -4.0, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
        assert_eq!(
            s2,
            Matrix4By4::new(
                RowOf4::new(1.0, 0.0, 0.0, 0.0),
                RowOf4::new(0.0, 1.0, 0.0, 0.0),
                RowOf4::new(0.0, 0.0, -9.0, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
    }

    #[test]
    fn x_rotation_matrix() {
        let xr1 = Matrix4By4::for_x_rotation(PI / 3.0);
        let xr2 = Matrix4By4::for_x_rotation(PI / 8.0);

        assert_eq!(
            xr1,
            Matrix4By4::new(
                RowOf4::new(1.0, 0.0, 0.0, 0.0),
                RowOf4::new(0.0, 0.5, -0.866025, 0.0),
                RowOf4::new(0.0, 0.866025, -0.5, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
        assert_eq!(
            xr2,
            Matrix4By4::new(
                RowOf4::new(1.0, 0.0, 0.0, 0.0),
                RowOf4::new(0.0, 0.923879, -0.382683, 0.0),
                RowOf4::new(0.0, 0.382683, -0.923879, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
    }

    #[test]
    fn y_rotation_matrix() {
        let yr1 = Matrix4By4::for_y_rotation(PI / 3.0);
        let yr2 = Matrix4By4::for_y_rotation(PI / 8.0);

        assert_eq!(
            yr1,
            Matrix4By4::new(
                RowOf4::new(0.5, 0.0, 0.866025, 0.0),
                RowOf4::new(0.0, 1.0, 0.0, 0.0),
                RowOf4::new(-0.866025, 0.0, 0.5, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
        assert_eq!(
            yr2,
            Matrix4By4::new(
                RowOf4::new(0.923879, 0.0, 0.382683, 0.0),
                RowOf4::new(0.0, 1.0, 0.0, 0.0),
                RowOf4::new(-0.382683, 0.0, 0.923879, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
    }

    #[test]
    fn z_rotation_matrix() {
        let zr1 = Matrix4By4::for_z_rotation(PI / 3.0);
        let zr2 = Matrix4By4::for_z_rotation(PI / 8.0);

        assert_eq!(
            zr1,
            Matrix4By4::new(
                RowOf4::new(0.5, -0.866025, 0.0, 0.0),
                RowOf4::new(0.866025, 0.5, 0.0, 0.0),
                RowOf4::new(0.0, 0.0, 1.0, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
        assert_eq!(
            zr2,
            Matrix4By4::new(
                RowOf4::new(0.923879, -0.382683, 0.0, 0.0),
                RowOf4::new(0.382683, 0.923879, 0.0, 0.0),
                RowOf4::new(0.0, 0.0, 1.0, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
    }

    #[test]
    fn shearing_matrix() {
        let sh1 = Matrix4By4::for_shearing(0.0, 0.1, 0.2, 0.3, 0.4, 0.5);
        let sh2 = Matrix4By4::for_shearing(0.8, 0.5, 0.3, 0.2, 0.1, 0.1);

        assert_eq!(
            sh1,
            Matrix4By4::new(
                RowOf4::new(1.0, 0.0, 0.1, 0.0),
                RowOf4::new(0.2, 1.0, 0.3, 0.0),
                RowOf4::new(0.4, 0.5, 1.0, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
        assert_eq!(
            sh2,
            Matrix4By4::new(
                RowOf4::new(1.0, 0.8, 0.5, 0.0),
                RowOf4::new(0.3, 1.0, 0.2, 0.0),
                RowOf4::new(0.1, 0.1, 1.0, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
    }

    #[test]
    fn transformation_matrix_for_the_default_orientation() {
        let from_point = Point3D::new(0.0, 0.0, 0.0);
        let to_point = Point3D::new(0.0, 0.0, -1.0);
        let up_vector = Vector3D::new(0.0, 1.0, 0.0);

        let matrix = Matrix4By4::view_transform(&from_point, &to_point, &up_vector);

        assert_eq!(matrix, Matrix4By4::identity());
    }

    #[test]
    fn transformation_matrix_looking_in_positive_z_direction() {
        let from_point = Point3D::new(0.0, 0.0, 0.0);
        let to_point = Point3D::new(0.0, 0.0, 1.0);
        let up_vector = Vector3D::new(0.0, 1.0, 0.0);

        let matrix = Matrix4By4::view_transform(&from_point, &to_point, &up_vector);

        assert_eq!(matrix, Matrix4By4::for_scaling(-1.0, 1.0, -1.0));
    }

    #[test]
    fn view_transformation_moves_the_world() {
        let from_point = Point3D::new(0.0, 0.0, 8.0);
        let to_point = Point3D::new(0.0, 0.0, 0.0);
        let up_vector = Vector3D::new(0.0, 1.0, 0.0);

        let matrix = Matrix4By4::view_transform(&from_point, &to_point, &up_vector);

        assert_eq!(matrix, Matrix4By4::for_translating(0.0, 0.0, -8.0));
    }

    #[test]
    fn arbitrary_view_transformation() {
        let from_point = Point3D::new(1.0, 3.0, 2.0);
        let to_point = Point3D::new(4.0, -2.0, 8.0);
        let up_vector = Vector3D::new(1.0, 1.0, 0.0);

        let matrix = Matrix4By4::view_transform(&from_point, &to_point, &up_vector);

        assert_eq!(
            matrix,
            Matrix4By4::new(
                RowOf4::new(-0.50709, 0.50709, 0.67612, -2.36643),
                RowOf4::new(0.76772, 0.60609, 0.12122, -2.82843),
                RowOf4::new(-0.35857, 0.59761, -0.71714, 0.0),
                RowOf4::new(0.0, 0.0, 0.0, 1.0),
            )
        );
    }
}
