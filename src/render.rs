use std::fmt::Write;

pub struct PPM {
    mainstream: String,
    substream: String,
}

impl PPM {
    pub fn new() -> Self {
        PPM {
            mainstream: String::new(),
            substream: String::new(),
        }
    }

    pub fn read(&mut self) -> Result<String, std::fmt::Error> {
        let mut stream = String::new();

        write!(stream, "{}", self.mainstream)?;
        write!(stream, "{}", self.substream)?;
        writeln!(stream)?;

        Ok(stream.to_string())
    }

    pub fn write_next(&mut self, next: &str) -> Result<(), std::fmt::Error> {
        // if length of "{substream} {next}" overshoots 70
        //    start new line
        // if there's already an element on line
        //    insert space to separate it from next
        // write next into substream

        if self.substream.len() + next.len() + 1 > 70 {
            self.flush_substream()?;
        }

        if !self.substream.is_empty() {
            write!(self.substream, " ")?;
        }

        write!(self.substream, "{}", next)?;
        Ok(())
    }

    fn flush_substream(&mut self) -> Result<(), std::fmt::Error> {
        writeln!(self.mainstream, "{}", self.substream)?;
        self.substream = String::new();
        Ok(())
    }

    pub fn insert_linefeed(&mut self) -> Result<(), std::fmt::Error> {
        self.flush_substream()
    }
}
