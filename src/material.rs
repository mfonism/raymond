use crate::color::Color;
use crate::d3::{Point3D, Vector3D};
use crate::light::PointLight;
use crate::math::compare_floats;
use crate::pattern::Pattern;
use crate::shape::Shape;

#[derive(Debug, Clone)]
pub struct Material {
    pub color: Color,
    pub ambient: f64,
    pub diffuse: f64,
    pub specular: f64,
    pub shininess: f64,
    pub pattern: Option<Pattern>,
}

impl Material {
    pub fn new() -> Self {
        Self {
            color: Color::new(1.0, 1.0, 1.0),
            ambient: 0.1,
            diffuse: 0.9,
            specular: 0.9,
            shininess: 200.0,
            pattern: None,
        }
    }

    pub fn lighting(
        &self,
        object: &Shape,
        light: &PointLight,
        point: &Point3D,
        eye_vector: &Vector3D,
        normal_vector: &Vector3D,
        in_shadow: bool,
    ) -> Color {
        let color = match &self.pattern {
            None => self.color.clone(),
            Some(pattern) => pattern.pattern_on_object_at(object, point),
        };

        // combine the color with the light's color/intensity
        let effective_color = &color * &light.intensity;

        // find the direction to the light source
        let light_vector = (&light.position - point).normalized();

        // compute the ambient contribution
        let ambient = &effective_color * self.ambient;

        // light_dot_normal represents the cosine of the angle between the
        // light vector and the normal vector. A negative number means the
        // light is on the other side of the surface.
        let light_dot_normal = light_vector.dot(normal_vector);

        if in_shadow || light_dot_normal < 0.0 {
            return ambient;
        }

        // compute the diffuse contribution
        let diffuse = &effective_color * (self.diffuse * light_dot_normal);

        // reflect_dot_eye represents the cosine of the angle between the
        // reflection vector and the eye vector. A negative number means the
        // light reflects away from the eye.
        let reflectv = (-&light_vector).reflect_around(normal_vector);
        let reflect_dot_eye = reflectv.dot(eye_vector);

        let specular = match reflect_dot_eye <= 0.0 {
            true => Color::new(0.0, 0.0, 0.0),
            _ => {
                let factor = reflect_dot_eye.powf(self.shininess);
                &light.intensity * (self.specular * factor)
            }
        };

        &ambient + &(&diffuse + &specular)
    }

    pub fn set_pattern(&mut self, pattern: Pattern) -> Option<Pattern> {
        std::mem::replace(&mut self.pattern, Some(pattern))
    }

    pub fn clear_pattern(&mut self) -> Option<Pattern> {
        std::mem::replace(&mut self.pattern, None)
    }
}

impl std::cmp::PartialEq for Material {
    fn eq(&self, other: &Self) -> bool {
        self.color == other.color
            && compare_floats(self.ambient, other.ambient)
            && compare_floats(self.diffuse, other.diffuse)
            && compare_floats(self.specular, other.specular)
            && compare_floats(self.shininess, other.shininess)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::pattern::PatternKind;

    #[test]
    fn test_default_material() {
        let material = Material::new();

        assert_eq!(material.color, Color::new(1.0, 1.0, 1.0));
        assert_eq!(material.ambient, 0.1);
        assert_eq!(material.diffuse, 0.9);
        assert_eq!(material.specular, 0.9);
        assert_eq!(material.shininess, 200.0);
    }

    #[test]
    fn test_material_equality() {
        assert_eq!(Material::new(), Material::new());
    }

    #[test]
    fn test_lighting_with_eye_between_light_and_surface() {
        let material = Material::new();
        let pos = Point3D::new(0.0, 0.0, 0.0);

        let eyev = Vector3D::new(0.0, 0.0, -1.0);
        let normalv = Vector3D::new(0.0, 0.0, -1.0);
        let light = PointLight::new(Point3D::new(0.0, 0.0, -10.0), Color::new(1.0, 1.0, 1.0));

        let lit = material.lighting(&Shape::sphere(), &light, &pos, &eyev, &normalv, false);

        assert_eq!(lit, Color::new(1.9, 1.9, 1.9));
    }

    #[test]
    fn test_lighting_with_eye_offset_45deg_to_surface() {
        let material = Material::new();
        let pos = Point3D::new(0.0, 0.0, 0.0);

        let eyev = Vector3D::new(0.0, (2.0_f64).sqrt() / 2.0, -(2.0_f64).sqrt() / 2.0);
        let normalv = Vector3D::new(0.0, 0.0, -1.0);
        let light = PointLight::new(Point3D::new(0.0, 0.0, -10.0), Color::new(1.0, 1.0, 1.0));

        let lit = material.lighting(&Shape::sphere(), &light, &pos, &eyev, &normalv, false);

        assert_eq!(lit, Color::new(1.0, 1.0, 1.0));
    }

    #[test]
    fn test_lighting_with_light_offset_45deg_to_surface() {
        let material = Material::new();
        let pos = Point3D::new(0.0, 0.0, 0.0);

        let eyev = Vector3D::new(0.0, 0.0, -1.0);
        let normalv = Vector3D::new(0.0, 0.0, -1.0);
        let light = PointLight::new(Point3D::new(0.0, 10.0, -10.0), Color::new(1.0, 1.0, 1.0));

        let lit = material.lighting(&Shape::sphere(), &light, &pos, &eyev, &normalv, false);

        assert_eq!(lit, Color::new(0.7364, 0.7364, 0.7364));
    }

    #[test]
    fn test_lighting_with_eye_in_path_of_reflection_vector() {
        let material = Material::new();
        let pos = Point3D::new(0.0, 0.0, 0.0);

        let eyev = Vector3D::new(0.0, -(2.0_f64).sqrt() / 2.0, -(2.0_f64).sqrt() / 2.0);
        let normalv = Vector3D::new(0.0, 0.0, -1.0);
        let light = PointLight::new(Point3D::new(0.0, 10.0, -10.0), Color::new(1.0, 1.0, 1.0));

        let lit = material.lighting(&Shape::sphere(), &light, &pos, &eyev, &normalv, false);

        assert_eq!(lit, Color::new(1.636396, 1.636396, 1.636396));
    }

    #[test]
    fn test_lighting_with_light_behind_surface() {
        let material = Material::new();
        let pos = Point3D::new(0.0, 0.0, 0.0);

        let eyev = Vector3D::new(0.0, 0.0, -1.0);
        let normalv = Vector3D::new(0.0, 0.0, -1.0);
        let light = PointLight::new(Point3D::new(0.0, 0.0, 10.0), Color::new(1.0, 1.0, 1.0));

        let lit = material.lighting(&Shape::sphere(), &light, &pos, &eyev, &normalv, false);

        assert_eq!(lit, Color::new(0.1, 0.1, 0.1));
    }

    #[test]
    fn lighting_with_the_surface_in_shadow() {
        let material = Material::new();
        let pos = Point3D::new(0.0, 0.0, 0.0);

        let eyev = Vector3D::new(0.0, 0.0, -1.0);
        let normalv = Vector3D::new(0.0, 0.0, -1.0);
        let light = PointLight::new(Point3D::new(0.0, 0.0, -10.0), Color::new(1.0, 1.0, 1.0));
        let in_shadow = true;

        let lit = material.lighting(&Shape::sphere(), &light, &pos, &eyev, &normalv, in_shadow);

        assert_eq!(lit, Color::new(0.1, 0.1, 0.1));
    }

    #[test]
    fn setting_pattern() {
        let mut material = Material::new();
        let pattern = Pattern::stripe(Color::white(), Color::black());

        let old_pattern = material.set_pattern(pattern);

        assert!(old_pattern.is_none());
        assert_eq!(
            material.pattern.unwrap().kind,
            PatternKind::Stripe {
                a: Color::white(),
                b: Color::black()
            }
        );
    }

    #[test]
    fn clearing_pattern() {
        let mut material = Material::new();
        material.set_pattern(Pattern::stripe(Color::white(), Color::black()));

        let old_pattern = material.clear_pattern();

        assert!(material.pattern.is_none());
        assert_eq!(
            old_pattern.unwrap().kind,
            PatternKind::Stripe {
                a: Color::white(),
                b: Color::black()
            }
        );
    }

    #[test]
    fn lighting_with_a_pattern_applied() {
        let mut material = Material::new();
        material.pattern = Some(Pattern::stripe(Color::white(), Color::black()));
        material.ambient = 1.0;
        material.diffuse = 0.0;
        material.specular = 0.0;
        let eye_vector = Vector3D::new(0.0, 0.0, -1.0);
        let normal_vector = Vector3D::new(0.0, 0.0, -1.0);
        let light = PointLight::new(Point3D::new(0.0, 0.0, -10.0), Color::white());

        let color_a = material.lighting(
            &Shape::sphere(),
            &light,
            &Point3D::new(0.9, 0.0, 0.0),
            &eye_vector,
            &normal_vector,
            false,
        );
        let color_b = material.lighting(
            &Shape::sphere(),
            &light,
            &Point3D::new(1.1, 0.0, 0.0),
            &eye_vector,
            &normal_vector,
            false,
        );

        assert_eq!(color_a, Color::white());
        assert_eq!(color_b, Color::black());
    }
}
