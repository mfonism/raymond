use crate::d3::{Point3D, Vector3D};
use crate::math::{compare_floats, EPSILON};
use crate::matrix::Matrix4By4;
use crate::shape::Shape;
use crate::world::World;

#[derive(Debug, Clone)]
pub struct Ray {
    pub origin: Point3D,
    pub direction: Vector3D,
}

#[derive(Debug)]
pub struct Intersection {
    pub t: f64,
    pub object: Shape,
}

pub struct Computation {
    pub t: f64,
    pub object: Shape,
    pub point: Point3D,
    pub eye_vector: Vector3D,
    pub normal_vector: Vector3D,
    pub inside: bool,
    pub over_point: Point3D,
}

impl Ray {
    pub fn new(origin: Point3D, direction: Vector3D) -> Self {
        Self { origin, direction }
    }

    pub fn positioned(&self, t: f64) -> Point3D {
        &self.origin + &(&self.direction * t)
    }

    pub fn intersect(&self, object: &mut Shape) -> Vec<Intersection> {
        // `object` knows how much it has been transformed in the world space
        // apply the inverse of this transformation to take it back to object space
        let normalized_ray = self.transform(&object.transform.inverted());

        object.intersect_local_ray(&normalized_ray)
    }

    pub fn intersect_world(&self, world: &mut World) -> Vec<Intersection> {
        let mut res = vec![];

        world
            .objects
            .iter_mut()
            .for_each(|object: &mut Shape| res.extend(self.intersect(object)));

        res.sort_by(|a, b| a.t.partial_cmp(&b.t).unwrap());

        res
    }

    pub fn transform(&self, matrix: &Matrix4By4) -> Self {
        Self::new(matrix * &self.origin, matrix * &self.direction)
    }
}

impl Intersection {
    pub fn new(t: f64, object: Shape) -> Self {
        Self { t, object }
    }

    pub fn hit(intersections: &[Self]) -> Option<Self> {
        let res = intersections
            .iter()
            .filter(|intersection| intersection.t.signum() > 0.0)
            .reduce(|a, b| if a.t <= b.t { a } else { b });

        if let Some(intersection) = res {
            return Some(Self::new(intersection.t, intersection.object.clone()));
        }

        None
    }

    pub fn prepare_computations(&self, ray: &Ray) -> Computation {
        let ray_positioned = ray.positioned(self.t);
        let mut computation = Computation::new(
            self.t,
            self.object.clone(),
            ray_positioned.clone(),
            -&ray.direction,
            self.object.normal_at(&ray_positioned),
            Point3D::new(0.0, 0.0, 0.0),
        );

        if computation.normal_vector.dot(&computation.eye_vector) < 0.0 {
            computation.inside = true;
            computation.normal_vector = -&computation.normal_vector;
        }

        computation.over_point = &computation.point + &(&computation.normal_vector * EPSILON);

        computation
    }
}

impl Computation {
    pub fn new(
        t: f64,
        object: Shape,
        point: Point3D,
        eye_vector: Vector3D,
        normal_vector: Vector3D,
        over_point: Point3D,
    ) -> Self {
        Self {
            t,
            object,
            point,
            eye_vector,
            normal_vector,
            inside: false,
            over_point,
        }
    }
}

impl std::cmp::PartialEq for Ray {
    fn eq(&self, other: &Self) -> bool {
        self.origin == other.origin && self.direction == other.direction
    }
}

impl std::cmp::PartialEq for Intersection {
    fn eq(&self, other: &Self) -> bool {
        compare_floats(self.t, other.t) && (self.object == other.object)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::assert_eqf;
    use crate::shape;

    #[test]
    fn creating_and_querying_a_ray() {
        let origin = Point3D::new(1.0, 2.0, 3.0);
        let direction = Vector3D::new(4.0, 5.0, 6.0);
        let ray = Ray::new(origin, direction);

        assert_eq!(ray.origin, Point3D::new(1.0, 2.0, 3.0));
        assert_eq!(ray.direction, Vector3D::new(4.0, 5.0, 6.0));
    }

    #[test]
    fn computing_a_point_from_a_distance() {
        let ray = Ray::new(Point3D::new(2.0, 3.0, 4.0), Vector3D::new(1.0, 0.0, 0.0));

        assert_eq!(ray.positioned(0.0), Point3D::new(2.0, 3.0, 4.0));
        assert_eq!(ray.positioned(1.0), Point3D::new(3.0, 3.0, 4.0));
        assert_eq!(ray.positioned(-1.0), Point3D::new(1.0, 3.0, 4.0));
        assert_eq!(ray.positioned(2.5), Point3D::new(4.5, 3.0, 4.0));
    }

    #[test]
    fn ray_intersection_of_sphere() {
        let ray = Ray::new(Point3D::new(0.0, 0.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));
        let mut sphere = Shape::sphere();

        let intersections = ray.intersect(&mut sphere);

        assert_eq!(intersections.len(), 2);
        assert_eqf!(intersections[0].t, 4.0);
        assert_eq!(intersections[0].object, sphere);
        assert_eqf!(intersections[1].t, 6.0);
        assert_eq!(intersections[1].object, sphere);
    }

    #[test]
    fn ray_intersection_of_sphere_at_tangent() {
        let ray = Ray::new(Point3D::new(0.0, 1.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));
        let mut sphere = Shape::sphere();

        let intersections = ray.intersect(&mut sphere);

        assert_eq!(intersections.len(), 2);
        assert_eqf!(intersections[0].t, 5.0);
        assert_eq!(intersections[0].object, sphere);
        assert_eqf!(intersections[1].t, 5.0);
        assert_eq!(intersections[1].object, sphere);
    }

    #[test]
    fn ray_non_interaction_with_sphere() {
        let ray = Ray::new(Point3D::new(0.0, 2.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));
        let mut sphere = Shape::sphere();

        let intersections = ray.intersect(&mut sphere);

        assert_eq!(intersections.len(), 0);
    }

    #[test]
    fn ray_intersection_of_sphere_from_within() {
        let ray = Ray::new(Point3D::new(0.0, 0.0, 0.0), Vector3D::new(0.0, 0.0, 1.0));
        let mut sphere = Shape::sphere();

        let intersections = ray.intersect(&mut sphere);

        assert_eq!(intersections.len(), 2);
        assert_eqf!(intersections[0].t, -1.0);
        assert_eq!(intersections[0].object, sphere);
        assert_eqf!(intersections[1].t, 1.0);
        assert_eq!(intersections[1].object, sphere);
    }

    #[test]
    fn ray_intersection_of_sphere_behind() {
        let ray = Ray::new(Point3D::new(0.0, 0.0, 5.0), Vector3D::new(0.0, 0.0, 1.0));
        let mut sphere = Shape::sphere();

        let intersections = ray.intersect(&mut sphere);

        assert_eq!(intersections.len(), 2);
        assert_eqf!(intersections[0].t, -6.0);
        assert_eq!(intersections[0].object, sphere);
        assert_eqf!(intersections[1].t, -4.0);
        assert_eq!(intersections[1].object, sphere);
    }

    #[test]
    fn ray_intersection_of_scaled_shape() {
        // using mock shape
        let ray = Ray::new(Point3D::new(0.0, 0.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));
        let mut shape = Shape::stub();
        shape.set_transform(Matrix4By4::for_scaling(2.0, 2.0, 2.0));

        ray.intersect(&mut shape);

        let normalized_local_ray =
            Ray::new(Point3D::new(0.0, 0.0, -2.5), Vector3D::new(0.0, 0.0, 0.5));
        assert_eq!(
            shape.kind,
            shape::Kind::Stub {
                local_ray: normalized_local_ray
            }
        );
    }

    #[test]
    fn ray_intersection_of_translated_shape() {
        // using mock shape
        let ray = Ray::new(Point3D::new(0.0, 0.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));
        let mut shape = Shape::stub();
        shape.set_transform(Matrix4By4::for_translating(5.0, 0.0, 0.0));

        ray.intersect(&mut shape);

        let normalized_local_ray =
            Ray::new(Point3D::new(-5.0, 0.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));
        assert_eq!(
            shape.kind,
            shape::Kind::Stub {
                local_ray: normalized_local_ray
            }
        );
    }

    #[test]
    fn creation_of_intersection() {
        let intersection = Intersection::new(3.5, Shape::sphere());

        assert_eqf!(intersection.t, 3.5);
        assert_eq!(intersection.object, Shape::sphere());
    }

    #[test]
    fn equality_of_intersections() {
        let intersection_a = Intersection::new(3.5, Shape::sphere());
        let intersection_b = Intersection::new(3.5, Shape::sphere());

        assert_eq!(intersection_a, intersection_b);
    }

    #[test]
    fn hit_when_all_intersections_have_positive_t() {
        let sphere = Shape::sphere();
        let intersection_a = Intersection::new(1.0, sphere.clone());
        let intersection_b = Intersection::new(2.0, sphere.clone());
        let intersections = vec![intersection_b, intersection_a];

        assert_eq!(
            Intersection::hit(&intersections),
            Some(Intersection::new(1.0, sphere))
        );
    }

    #[test]
    fn hit_intersections_have_equal_but_opposite_t() {
        let sphere = Shape::sphere();
        let intersection_a = Intersection::new(-1.0, sphere.clone());
        let intersection_b = Intersection::new(1.0, sphere.clone());
        let intersections = vec![intersection_b, intersection_a];

        assert_eq!(
            Intersection::hit(&intersections),
            Some(Intersection::new(1.0, sphere))
        );
    }

    #[test]
    fn hit_when_all_intersections_have_negative_t() {
        let sphere = Shape::sphere();
        let intersection_a = Intersection::new(-2.0, sphere.clone());
        let intersection_b = Intersection::new(-1.0, sphere);
        let intersections = vec![intersection_b, intersection_a];

        assert_eq!(Intersection::hit(&intersections), None);
    }

    #[test]
    fn hit_is_always_the_lowest_nonnegative_intersection() {
        let sphere = Shape::sphere();
        let intersection_a = Intersection::new(5.0, sphere.clone());
        let intersection_b = Intersection::new(7.0, sphere.clone());
        let intersection_c = Intersection::new(-3.0, sphere.clone());
        let intersection_d = Intersection::new(2.0, sphere.clone());
        let intersections = vec![
            intersection_a,
            intersection_b,
            intersection_c,
            intersection_d,
        ];

        assert_eq!(
            Intersection::hit(intersections.as_slice()),
            Some(Intersection::new(2.0, sphere))
        );
    }

    #[test]
    fn translating_a_ray() {
        let ray = Ray::new(Point3D::new(1.0, 2.0, 3.0), Vector3D::new(0.0, 1.0, 0.0));
        let matrix = Matrix4By4::for_translating(3.0, 4.0, 5.0);

        let new_ray = ray.transform(&matrix);

        assert_eq!(new_ray.origin, Point3D::new(4.0, 6.0, 8.0));
        assert_eq!(new_ray.direction, Vector3D::new(0.0, 1.0, 0.0));
    }

    #[test]
    fn scaling_a_ray() {
        let ray = Ray::new(Point3D::new(1.0, 2.0, 3.0), Vector3D::new(0.0, 1.0, 0.0));
        let matrix = Matrix4By4::for_scaling(2.0, 3.0, 4.0);

        let new_ray = ray.transform(&matrix);

        assert_eq!(new_ray.origin, Point3D::new(2.0, 6.0, 12.0));
        assert_eq!(new_ray.direction, Vector3D::new(0.0, 3.0, 0.0));
    }

    #[test]
    fn ray_intersection_of_world() {
        let mut world = World::new();
        let ray = Ray::new(Point3D::new(0.0, 0.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));

        let intersections = ray.intersect_world(&mut world);

        assert_eq!(intersections.len(), 4);
        assert_eq!(intersections[0].t, 4.0);
        assert_eq!(intersections[1].t, 4.5);
        assert_eq!(intersections[2].t, 5.5);
        assert_eq!(intersections[3].t, 6.0);
    }

    #[test]
    fn precomputing_the_state_of_an_intersection() {
        let ray = Ray::new(Point3D::new(0.0, 0.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));
        let intersection = Intersection::new(4.0, Shape::sphere());

        let computation = intersection.prepare_computations(&ray);

        assert_eq!(computation.t, intersection.t);
        assert_eq!(computation.object, intersection.object);
        assert_eq!(computation.point, Point3D::new(0.0, 0.0, -1.0));
        assert_eq!(computation.eye_vector, Vector3D::new(0.0, 0.0, -1.0));
        assert_eq!(computation.normal_vector, Vector3D::new(0.0, 0.0, -1.0));
    }

    #[test]
    fn pre_computation_when_intersection_occurs_outside_shape() {
        let ray = Ray::new(Point3D::new(0.0, 0.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));
        let intersection = Intersection::new(4.0, Shape::sphere());

        let computation = intersection.prepare_computations(&ray);

        assert!(!computation.inside);
    }

    #[test]
    fn pre_computation_when_intersection_occurs_inside_shape() {
        let ray = Ray::new(Point3D::new(0.0, 0.0, 0.0), Vector3D::new(0.0, 0.0, 1.0));
        let intersection = Intersection::new(1.0, Shape::sphere());

        let computation = intersection.prepare_computations(&ray);

        assert_eq!(computation.point, Point3D::new(0.0, 0.0, 1.0));
        assert_eq!(computation.eye_vector, Vector3D::new(0.0, 0.0, -1.0));
        assert!(computation.inside);
        // normal would have been (0, 0, 1), but is inverted
        // because intersection is inside sphere!
        assert_eq!(computation.normal_vector, Vector3D::new(0.0, 0.0, -1.0));
    }

    #[test]
    fn computations_take_some_epsilon_into_consideration() {
        let ray = Ray::new(Point3D::new(0.0, 0.0, -5.0), Vector3D::new(0.0, 0.0, 1.0));
        let mut shape = Shape::sphere();
        shape.transform = Matrix4By4::for_translating(0.0, 0.0, 1.0);
        let intersection = Intersection::new(5.0, shape);

        let computations = intersection.prepare_computations(&ray);

        assert!(computations.over_point.z < -EPSILON / 2.0);
        assert!(computations.point.z > computations.over_point.z);
    }
}
