use crate::color::Color;
use crate::render::PPM;
use std::collections::HashMap;

pub struct Canvas {
    pub width: u32,
    pub height: u32,
    pixel_map: HashMap<PixelCoord, Color>,
}

#[derive(PartialEq, Eq, Hash)]
struct PixelCoord {
    x: u32,
    y: u32,
}

impl Canvas {
    pub fn new(width: u32, height: u32) -> Self {
        Self {
            width,
            height,
            pixel_map: HashMap::new(),
        }
    }

    pub fn write_pixel(&mut self, x: u32, y: u32, color: Color) -> Option<Color> {
        self.pixel_map.insert(PixelCoord::new(x, y), color)
    }

    pub fn pixel_at(&self, x: u32, y: u32) -> Option<&Color> {
        self.pixel_map.get(&PixelCoord::new(x, y))
    }

    pub fn as_ppm(&self) -> Result<PPM, std::fmt::Error> {
        let mut ppm = PPM::new();

        // write PPM header
        ppm.write_next(&format!("P3\n{} {}\n255", self.width, self.height))?;
        ppm.insert_linefeed()?;

        // write PPM body
        let black = Color::new(0.0, 0.0, 0.0);
        for y in 0..self.height {
            for x in 0..self.width {
                let color = self.pixel_at(x, y).unwrap_or(&black);
                let color = color.contained(0, 255);
                ppm.write_next(&format!("{}", color.red))?;
                ppm.write_next(&format!("{}", color.green))?;
                ppm.write_next(&format!("{}", color.blue))?;
            }
            ppm.insert_linefeed()?;
        }
        Ok(ppm)
    }
}

impl PixelCoord {
    fn new(x: u32, y: u32) -> Self {
        Self { x, y }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::assert_lte;

    #[test]
    fn canvas_creation() {
        let canvas = Canvas::new(10, 20);

        assert_eq!(canvas.width, 10);
        assert_eq!(canvas.height, 20);
        assert!(canvas.pixel_map.is_empty());
    }

    #[test]
    fn pixel_coord_creation() {
        let coord = PixelCoord::new(3, 4);

        assert_eq!(coord.x, 3);
        assert_eq!(coord.y, 4);
    }

    #[test]
    fn writing_pixel_to_canvas() {
        let mut canvas = Canvas::new(10, 20);
        let red = Color::new(1.0, 0.0, 0.0);

        let overwritten_pixel = canvas.write_pixel(2, 3, red);

        assert_eq!(overwritten_pixel, None);
        assert_eq!(canvas.pixel_map.len(), 1);
        // red has been inserted into the appropriate coord in the pixel map
        assert_eq!(canvas.pixel_at(2, 3), Some(&Color::new(1.0, 0.0, 0.0)));
    }

    #[test]
    fn ppm_header_construction() {
        let canvas = Canvas::new(5, 3);

        let mut ppm = canvas.as_ppm().unwrap();
        let ppm = ppm.read().unwrap();
        let mut ppm_lines = ppm.lines();

        assert_eq!(ppm_lines.next(), Some("P3"));
        assert_eq!(ppm_lines.next(), Some("5 3"));
        assert_eq!(ppm_lines.next(), Some("255"));
    }

    #[test]
    fn ppm_body_construction() {
        let mut canvas = Canvas::new(5, 3);
        let color1 = Color::new(1.5, 0.0, 0.0);
        let color2 = Color::new(0.0, 0.5, 0.0);
        let color3 = Color::new(-0.5, 0.0, 1.0);

        canvas.write_pixel(0, 0, color1);
        canvas.write_pixel(2, 1, color2);
        canvas.write_pixel(4, 2, color3);

        let mut ppm = canvas.as_ppm().unwrap();
        let ppm = ppm.read().unwrap();
        let mut ppm_lines = ppm.lines().skip(3);

        assert_eq!(ppm_lines.next(), Some("255 0 0 0 0 0 0 0 0 0 0 0 0 0 0"));
        assert_eq!(ppm_lines.next(), Some("0 0 0 0 0 0 0 128 0 0 0 0 0 0 0"));
        assert_eq!(ppm_lines.next(), Some("0 0 0 0 0 0 0 0 0 0 0 0 0 0 255"));
    }

    #[test]
    fn ppm_lines_do_not_exceed_70_chars() {
        let mut canvas = Canvas::new(10, 1);
        // set every pixel to white so each line in naive ppm would be len 119
        for y in 0..=canvas.height {
            for x in 0..=canvas.width {
                canvas.write_pixel(x, y, Color::new(1.0, 1.0, 1.0));
            }
        }

        for line in canvas.as_ppm().unwrap().read().unwrap().lines() {
            assert_lte!(line.len(), 70);
        }
    }

    #[test]
    fn last_char_in_ppm_is_linefeed() {
        let canvas = Canvas::new(5, 3);

        assert_eq!(
            canvas
                .as_ppm()
                .unwrap()
                .read()
                .unwrap()
                .chars()
                .last()
                .unwrap(),
            '\n'
        );
    }
}
