use crate::macros;

pub const EPSILON: f64 = 0.00001;

pub fn compare_floats(a: f64, b: f64) -> bool {
    macros::compare_floats(a, b)
}

#[cfg(test)]
mod test {
    use super::compare_floats;

    #[test]
    fn float_comparison() {
        let base_num = 1.0;
        let epsilon = 0.00001;
        let lesser_than_epsilon = epsilon / 10.0;
        let greater_than_epsilon = epsilon + lesser_than_epsilon;

        assert!(compare_floats(base_num, base_num + lesser_than_epsilon));
        assert!(compare_floats(base_num + lesser_than_epsilon, base_num));

        assert!(!compare_floats(base_num, base_num + epsilon));
        assert!(!compare_floats(base_num + epsilon, base_num));
        assert!(!compare_floats(base_num, base_num + greater_than_epsilon));
        assert!(!compare_floats(base_num + greater_than_epsilon, base_num));
    }
}
