pub fn compare_floats(a: f64, b: f64) -> bool {
    (a - b).abs() < 0.00001
}

#[macro_export]
macro_rules! assert_eqf {
    ($left:expr, $right:expr $(,)?) => {{
        match (&$left, &$right) {
            (left_val, right_val) => {
                if !(compare_floats(*left_val, *right_val)) {
                    panic!("assert_eqf({:?}, {:?})", left_val, right_val);
                }
            }
        }
    }};
}

#[macro_export]
macro_rules! assert_lte {
    ($left:expr, $right:expr $(,)?) => {{
        match (&$left, &$right) {
            (left_val, right_val) => {
                if !(left_val <= right_val) {
                    panic!("assert_lte({:?}, {:?})", left_val, right_val);
                }
            }
        }
    }};
}
