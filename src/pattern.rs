use crate::color::Color;
use crate::d3::Point3D;
use crate::matrix::Matrix4By4;
use crate::shape::Shape;

#[derive(Debug, Clone)]
pub struct Pattern {
    pub transform: Matrix4By4,
    pub kind: PatternKind,
}

#[derive(Debug, Clone, PartialEq)]
pub enum PatternKind {
    Stub,
    Stripe { a: Color, b: Color },
    Gradient { a: Color, b: Color },
    Ring { a: Color, b: Color },
    Checker { a: Color, b: Color },
}

impl Pattern {
    pub fn with_kind(kind: PatternKind) -> Self {
        Self {
            transform: Matrix4By4::identity(),
            kind,
        }
    }

    pub fn stub() -> Self {
        Self::with_kind(PatternKind::Stub)
    }

    pub fn stripe(a: Color, b: Color) -> Self {
        Self::with_kind(PatternKind::Stripe { a, b })
    }

    pub fn gradient(a: Color, b: Color) -> Self {
        Self::with_kind(PatternKind::Gradient { a, b })
    }

    pub fn ring(a: Color, b: Color) -> Self {
        Self::with_kind(PatternKind::Ring { a, b })
    }

    pub fn checker(a: Color, b: Color) -> Self {
        Self::with_kind(PatternKind::Checker { a, b })
    }

    pub fn set_transform(&mut self, transform: Matrix4By4) {
        self.transform = transform;
    }

    pub fn pattern_at(&self, point: &Point3D) -> Color {
        match &self.kind {
            PatternKind::Stripe { a, b } => match (point.x.floor().trunc().abs() as i64) % 2 {
                0 => a.clone(),
                _ => b.clone(),
            },
            PatternKind::Gradient { a, b } => a + &(&(b - a) * point.x.fract()),
            PatternKind::Ring { a, b } => {
                match (point.x.powf(2.0) + point.z.powf(2.0)).sqrt().floor() as i64 % 2 {
                    0 => a.clone(),
                    _ => b.clone(),
                }
            }
            PatternKind::Checker { a, b } => {
                match (point.x.floor() + point.y.floor() + point.z.floor()) as i64 % 2 {
                    0 => a.clone(),
                    _ => b.clone(),
                }
            }
            PatternKind::Stub => Color::new(point.x, point.y, point.z),
        }
    }

    pub fn pattern_on_object_at(&self, object: &Shape, point: &Point3D) -> Color {
        let object_space_point = &object.transform.inverted() * point;
        let pattern_space_point = &self.transform.inverted() * &object_space_point;
        self.pattern_at(&pattern_space_point)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn default_pattern_transformation() {
        let stub = Pattern::stub();

        assert_eq!(stub.transform, Matrix4By4::identity());
    }

    #[test]
    fn assigning_a_transformation() {
        let mut stub = Pattern::stub();

        stub.set_transform(Matrix4By4::for_translating(1.0, 2.0, 3.0));

        assert_eq!(stub.transform, Matrix4By4::for_translating(1.0, 2.0, 3.0));
    }

    #[test]
    fn pattern_with_an_object_transformation() {
        let mut sphere = Shape::sphere();
        sphere.set_transform(Matrix4By4::for_scaling(2.0, 2.0, 2.0));
        let stub = Pattern::stub();

        let color = stub.pattern_on_object_at(&sphere, &Point3D::new(2.0, 3.0, 4.0));

        assert_eq!(color, Color::new(1.0, 1.5, 2.0));
    }

    #[test]
    fn pattern_with_a_pattern_transformation() {
        let sphere = Shape::sphere();
        let mut stub = Pattern::stub();
        stub.set_transform(Matrix4By4::for_scaling(2.0, 2.0, 2.0));

        let color = stub.pattern_on_object_at(&sphere, &Point3D::new(2.0, 3.0, 4.0));

        assert_eq!(color, Color::new(1.0, 1.5, 2.0));
    }

    #[test]
    fn pattern_with_both_an_object_and_a_pattern_transformation() {
        let mut sphere = Shape::sphere();
        sphere.set_transform(Matrix4By4::for_scaling(2.0, 2.0, 2.0));
        let mut stub = Pattern::stub();
        stub.set_transform(Matrix4By4::for_translating(0.5, 1.0, 1.5));

        let color = stub.pattern_on_object_at(&sphere, &Point3D::new(2.5, 3.0, 3.5));

        assert_eq!(color, Color::new(0.75, 0.5, 0.25));
    }

    #[test]
    fn creation_of_stripe_pattern() {
        let stripe = Pattern::stripe(Color::white(), Color::black());

        assert_eq!(
            stripe.kind,
            PatternKind::Stripe {
                a: Color::white(),
                b: Color::black()
            }
        );
    }

    #[test]
    fn stripe_pattern_is_constant_in_y() {
        let stripe = Pattern::stripe(Color::white(), Color::black());

        assert_eq!(
            stripe.pattern_at(&Point3D::new(0.0, 0.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            stripe.pattern_at(&Point3D::new(0.0, 1.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            stripe.pattern_at(&Point3D::new(0.0, 2.0, 0.0)),
            Color::white()
        );
    }

    #[test]
    fn stripe_pattern_is_constant_in_z() {
        let stripe = Pattern::stripe(Color::white(), Color::black());

        assert_eq!(
            stripe.pattern_at(&Point3D::new(0.0, 0.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            stripe.pattern_at(&Point3D::new(0.0, 0.0, 1.0)),
            Color::white()
        );
        assert_eq!(
            stripe.pattern_at(&Point3D::new(0.0, 0.0, 2.0)),
            Color::white()
        );
    }

    #[test]
    fn stripe_pattern_alternates_in_x() {
        let stripe = Pattern::stripe(Color::white(), Color::black());

        assert_eq!(
            stripe.pattern_at(&Point3D::new(0.0, 0.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            stripe.pattern_at(&Point3D::new(0.9, 0.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            stripe.pattern_at(&Point3D::new(1.0, 0.0, 0.0)),
            Color::black()
        );
        assert_eq!(
            stripe.pattern_at(&Point3D::new(-0.1, 0.0, 0.0)),
            Color::black()
        );
        assert_eq!(
            stripe.pattern_at(&Point3D::new(-1.0, 0.0, 0.0)),
            Color::black()
        );
        assert_eq!(
            stripe.pattern_at(&Point3D::new(-1.1, 0.0, 0.0)),
            Color::white()
        );
    }

    #[test]
    fn stripes_with_an_object_transformation() {
        let mut sphere = Shape::sphere();
        sphere.set_transform(Matrix4By4::for_scaling(2.0, 2.0, 2.0));
        let stripe = Pattern::stripe(Color::white(), Color::black());

        let color = stripe.pattern_on_object_at(&sphere, &Point3D::new(1.5, 0.0, 0.0));

        assert_eq!(color, Color::white());
    }

    #[test]
    fn stripes_with_a_pattern_transformation() {
        let sphere = Shape::sphere();
        let mut stripe = Pattern::stripe(Color::white(), Color::black());
        stripe.set_transform(Matrix4By4::for_scaling(2.0, 2.0, 2.0));

        let color = stripe.pattern_on_object_at(&sphere, &Point3D::new(1.5, 0.0, 0.0));

        assert_eq!(color, Color::white());
    }

    #[test]
    fn stripes_with_both_an_object_and_a_pattern_transformation() {
        let mut sphere = Shape::sphere();
        sphere.set_transform(Matrix4By4::for_scaling(2.0, 2.0, 2.0));
        let mut stripe = Pattern::stripe(Color::white(), Color::black());
        stripe.set_transform(Matrix4By4::for_translating(0.5, 0.0, 0.0));

        let color = stripe.pattern_on_object_at(&sphere, &Point3D::new(2.5, 0.0, 0.0));

        assert_eq!(color, Color::white());
    }

    #[test]
    fn gradient_linearly_interpolates_between_colors() {
        let gradient = Pattern::gradient(Color::white(), Color::black());

        assert_eq!(
            gradient.pattern_at(&Point3D::new(0.0, 0.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            gradient.pattern_at(&Point3D::new(0.25, 0.0, 0.0)),
            Color::new(0.75, 0.75, 0.75)
        );
        assert_eq!(
            gradient.pattern_at(&Point3D::new(0.5, 0.0, 0.0)),
            Color::new(0.5, 0.5, 0.5)
        );
        assert_eq!(
            gradient.pattern_at(&Point3D::new(0.75, 0.0, 0.0)),
            Color::new(0.25, 0.25, 0.25)
        );
    }

    #[test]
    fn rings_extend_in_both_x_and_z() {
        let ring = Pattern::ring(Color::white(), Color::black());

        assert_eq!(
            ring.pattern_at(&Point3D::new(0.0, 0.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            ring.pattern_at(&Point3D::new(1.0, 0.0, 0.0)),
            Color::black()
        );
        assert_eq!(
            ring.pattern_at(&Point3D::new(0.0, 0.0, 1.0)),
            Color::black()
        );
        // 0.708 = just slightly more than (2).sqrt() / 2
        assert_eq!(
            ring.pattern_at(&Point3D::new(0.708, 0.0, 0.708)),
            Color::black()
        );
    }

    #[test]
    fn checkers_should_repeat_in_x() {
        let checker = Pattern::checker(Color::white(), Color::black());

        assert_eq!(
            checker.pattern_at(&Point3D::new(0.0, 0.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            checker.pattern_at(&Point3D::new(0.99, 0.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            checker.pattern_at(&Point3D::new(1.01, 0.0, 0.0)),
            Color::black()
        );
    }

    #[test]
    fn checkers_should_repeat_in_y() {
        let checker = Pattern::checker(Color::white(), Color::black());

        assert_eq!(
            checker.pattern_at(&Point3D::new(0.0, 0.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            checker.pattern_at(&Point3D::new(0.0, 0.99, 0.0)),
            Color::white()
        );
        assert_eq!(
            checker.pattern_at(&Point3D::new(0.0, 1.01, 0.0)),
            Color::black()
        );
    }

    #[test]
    fn checkers_should_repeat_in_z() {
        let checker = Pattern::checker(Color::white(), Color::black());

        assert_eq!(
            checker.pattern_at(&Point3D::new(0.0, 0.0, 0.0)),
            Color::white()
        );
        assert_eq!(
            checker.pattern_at(&Point3D::new(0.0, 0.0, 0.99)),
            Color::white()
        );
        assert_eq!(
            checker.pattern_at(&Point3D::new(0.0, 0.0, 1.01)),
            Color::black()
        );
    }
}
