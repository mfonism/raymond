use crate::d3::{Point3D, Vector3D};
use crate::material::Material;
use crate::math::EPSILON;
use crate::matrix::{Matrix4By4, RowOf4};
use crate::ray::{Intersection, Ray};

#[derive(Debug, Clone)]
pub struct Shape {
    pub transform: Matrix4By4,
    pub material: Material,
    pub kind: Kind,
}

#[derive(Debug, Clone)]
pub enum Kind {
    Stub { local_ray: Ray },
    Sphere,
    Plane,
}

impl Shape {
    pub fn with_kind(kind: Kind) -> Self {
        Self {
            transform: Matrix4By4::identity(),
            material: Material::new(),
            kind,
        }
    }

    pub fn sphere() -> Self {
        Self::with_kind(Kind::Sphere)
    }

    pub fn plane() -> Self {
        Self::with_kind(Kind::Plane)
    }

    pub fn stub() -> Self {
        Self::with_kind(Kind::Stub {
            local_ray: Ray::new(Point3D::new(0.0, 0.0, 0.0), Vector3D::new(0.0, 0.0, 1.0)),
        })
    }

    pub fn set_transform(&mut self, transform: Matrix4By4) {
        self.transform = transform;
    }

    pub fn normal_at(&self, point: &Point3D) -> Vector3D {
        // point in object space
        let local_point = &self.transform.inverted() * point;
        let local_normal = self.local_normal_at(&local_point);

        // have to do this so we can manually make sure the w value
        // in the underlying RowOf4 object is always set to 0
        // because sometimes the calculations will make it slightly above 0
        // and will cause errors as our system views anything with
        // w != 0 as a point
        let mut world_normal: RowOf4 =
            &self.transform.inverted().transposed() * &local_normal.as_row_of_4();
        world_normal.3 = 0.0;
        let world_normal: Vector3D = Vector3D::from_row_of_4(&world_normal);
        world_normal.normalized()
    }

    pub fn local_normal_at(&self, local_point: &Point3D) -> Vector3D {
        // normal_at, localized to each kind of shape
        if let Kind::Plane = self.kind {
            return Vector3D::new(0.0, 1.0, 0.0);
        }
        local_point - &Point3D::new(0.0, 0.0, 0.0)
    }

    pub fn intersect_local_ray(&mut self, local_ray: &Ray) -> Vec<Intersection> {
        let mut res = Vec::new();

        if let Kind::Stub { .. } = self.kind {
            self.kind = Kind::Stub {
                local_ray: local_ray.clone(),
            };
            return Vec::new();
        }

        if let Kind::Plane = self.kind {
            if local_ray.direction.y.abs() < EPSILON {
                return Vec::new();
            }

            let t = -local_ray.origin.y / local_ray.direction.y;
            return vec![Intersection::new(t, self.clone())];
        }

        let distance_to_ray = &local_ray.origin - &Point3D::new(0.0, 0.0, 0.0);

        let a = local_ray.direction.dot(&local_ray.direction);
        let b = 2.0 * local_ray.direction.dot(&distance_to_ray);
        let c = distance_to_ray.dot(&distance_to_ray) - 1.0;

        let discriminant = b.powf(2.0) - (4.0 * a * c);

        if discriminant < 0.0 {
            return res;
        }

        let t1 = ((-b) - discriminant.sqrt()) / (2.0 * a);
        let t2 = ((-b) + discriminant.sqrt()) / (2.0 * a);

        // return world-space object (not object-space object) in intersections
        res.push(Intersection::new(t1, self.clone()));
        res.push(Intersection::new(t2, self.clone()));

        res
    }
}

impl std::cmp::PartialEq for Shape {
    fn eq(&self, other: &Self) -> bool {
        self.kind == other.kind && self.transform == other.transform
    }
}

impl std::cmp::PartialEq for Kind {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (
                Self::Stub {
                    local_ray: local_ray1,
                },
                Self::Stub {
                    local_ray: local_ray2,
                },
            ) => local_ray1 == local_ray2,
            (Self::Sphere, Self::Sphere) => true,
            (Self::Plane, Self::Plane) => true,
            _ => false,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::f64::consts::PI;

    #[test]
    fn transformation_on_shape() {
        // we're using a mock shape here
        let shape = Shape::stub();

        assert_eq!(shape.transform, Matrix4By4::identity());
    }

    #[test]
    fn assigning_transformation_to_shape() {
        // still using a mock shape here
        let mut shape = Shape::stub();

        shape.set_transform(Matrix4By4::for_translating(2.0, 3.0, 4.0));
        assert_eq!(shape.transform, Matrix4By4::for_translating(2.0, 3.0, 4.0));
    }

    #[test]
    fn default_material_on_shape() {
        // yup, mock shape
        let shape = Shape::stub();

        assert_eq!(shape.material, Material::new());
    }

    #[test]
    fn sphere_equality() {
        let mut sphere = Shape::sphere();

        assert_eq!(sphere, Shape::sphere());
        sphere.set_transform(Matrix4By4::for_translating(2.0, 3.0, 4.0));
        assert_ne!(sphere, Shape::sphere());
    }

    #[test]
    fn set_spheres_tranformation() {
        let mut sphere = Shape::sphere();
        let tx = Matrix4By4::for_translating(2.0, 3.0, 4.0);

        sphere.set_transform(tx);

        assert_eq!(sphere.transform, Matrix4By4::for_translating(2.0, 3.0, 4.0));
    }

    #[test]
    fn normal_on_a_sphere_at_point_on_x_axis() {
        let sphere = Shape::sphere();
        let point = Point3D::new(1.0, 0.0, 0.0);

        let normal_vector = sphere.normal_at(&point);

        assert_eq!(normal_vector, Vector3D::new(1.0, 0.0, 0.0));
        assert_eq!(normal_vector.normalized(), normal_vector);
    }

    #[test]
    fn normal_on_a_sphere_at_point_on_y_axis() {
        let sphere = Shape::sphere();
        let point = Point3D::new(0.0, 1.0, 0.0);

        let normal_vector = sphere.normal_at(&point);

        assert_eq!(normal_vector, Vector3D::new(0.0, 1.0, 0.0));
        assert_eq!(normal_vector.normalized(), normal_vector);
    }

    #[test]
    fn normal_on_a_sphere_at_point_on_z_axis() {
        let sphere = Shape::sphere();
        let point = Point3D::new(0.0, 0.0, 1.0);

        let normal_vector = sphere.normal_at(&point);

        assert_eq!(normal_vector, Vector3D::new(0.0, 0.0, 1.0));
        assert_eq!(normal_vector.normalized(), normal_vector);
    }

    #[test]
    fn normal_on_a_sphere_at_nonaxial_point() {
        let sphere = Shape::sphere();
        let num = (3.0_f64).sqrt() / 3.0;
        let point = Point3D::new(num, num, num);

        let normal_vector = sphere.normal_at(&point);

        assert_eq!(normal_vector, Vector3D::new(num, num, num));
        assert_eq!(normal_vector.normalized(), normal_vector);
    }

    #[test]
    fn normal_on_a_translated_sphere() {
        let mut sphere = Shape::sphere();
        sphere.set_transform(Matrix4By4::for_translating(0.0, 1.0, 0.0));
        let point = Point3D::new(0.0, 1.70711, -0.70711);

        let normal_vector = sphere.normal_at(&point);

        assert_eq!(normal_vector, Vector3D::new(0.0, 0.70711, -0.70711));
        assert_eq!(normal_vector.normalized(), normal_vector);
    }

    #[test]
    fn normal_on_a_transformed_sphere() {
        let mut sphere = Shape::sphere();
        let transform =
            &Matrix4By4::for_scaling(1.0, 0.5, 1.0) * &Matrix4By4::for_z_rotation(PI / 5.0);
        sphere.set_transform(transform);
        let point = Point3D::new(0.0, (2.0_f64).sqrt() / 2.0, -(2.0_f64).sqrt() / 2.0);

        let normal_vector = sphere.normal_at(&point);

        assert_eq!(normal_vector, Vector3D::new(0.0, 0.97014, -0.24254));
        assert_eq!(normal_vector.normalized(), normal_vector);
    }

    #[test]
    fn reflecting_a_vector_approaching_at_45deg() {
        let vector = Vector3D::new(1.0, -1.0, 0.0);
        let normal = Vector3D::new(0.0, 1.0, 0.0);

        assert_eq!(vector.reflect_around(&normal), Vector3D::new(1.0, 1.0, 0.0));
    }

    #[test]
    fn reflecting_a_vector_off_of_a_slanted_surface() {
        let vector = Vector3D::new(0.0, -1.0, 0.0);
        let normal = Vector3D::new((2.0_f64).sqrt() / 2.0, (2.0_f64).sqrt() / 2.0, 0.0);

        assert_eq!(vector.reflect_around(&normal), Vector3D::new(1.0, 0.0, 0.0));
    }

    #[test]
    fn the_normal_of_a_plane_is_constant_everywhere() {
        let plane = Shape::plane();
        let norm1 = plane.local_normal_at(&Point3D::new(0.0, 0.0, 0.0));
        let norm2 = plane.local_normal_at(&Point3D::new(10.0, 0.0, -10.0));
        let norm3 = plane.local_normal_at(&Point3D::new(-5.0, 0.0, 150.0));

        assert_eq!(norm1, Vector3D::new(0.0, 1.0, 0.0));
        assert_eq!(norm2, Vector3D::new(0.0, 1.0, 0.0));
        assert_eq!(norm3, Vector3D::new(0.0, 1.0, 0.0));
    }

    #[test]
    fn non_intersection_of_local_ray_parallel_to_plane() {
        let mut plane = Shape::plane();
        let local_ray = Ray::new(Point3D::new(0.0, 10.0, 0.0), Vector3D::new(0.0, 0.0, 1.0));

        let intersections = plane.intersect_local_ray(&local_ray);

        assert!(intersections.is_empty());
    }

    #[test]
    fn non_intersection_of_coplanar_local_ray() {
        let mut plane = Shape::plane();
        let local_ray = Ray::new(Point3D::new(0.0, 0.0, 0.0), Vector3D::new(0.0, 0.0, 1.0));

        let intersections = plane.intersect_local_ray(&local_ray);

        assert!(intersections.is_empty());
    }

    #[test]
    fn intersection_of_ray_downwards_shooting_ray_on_plane() {
        let mut plane = Shape::plane();
        let local_ray = Ray::new(Point3D::new(0.0, 1.0, 0.0), Vector3D::new(0.0, -1.0, 1.0));

        let intersections = plane.intersect_local_ray(&local_ray);

        assert_eq!(intersections.len(), 1);
        assert_eq!(intersections[0].t, 1.0);
        assert_eq!(intersections[0].object, plane);
    }

    #[test]
    fn intersection_of_ray_upwards_shooting_ray_under_plane() {
        let mut plane = Shape::plane();
        let local_ray = Ray::new(Point3D::new(0.0, -1.0, 0.0), Vector3D::new(0.0, 1.0, 1.0));

        let intersections = plane.intersect_local_ray(&local_ray);

        assert_eq!(intersections.len(), 1);
        assert_eq!(intersections[0].t, 1.0);
        assert_eq!(intersections[0].object, plane);
    }
}
