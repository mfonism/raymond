use crate::math::compare_floats;

#[derive(Debug, Clone)]
pub struct Color {
    pub red: f64,
    pub green: f64,
    pub blue: f64,
}

impl Color {
    pub fn new(red: f64, green: f64, blue: f64) -> Self {
        Self { red, green, blue }
    }

    pub fn black() -> Self {
        Self::new(0.0, 0.0, 0.0)
    }

    pub fn white() -> Self {
        Self::new(1.0, 1.0, 1.0)
    }

    fn contain_val(lower_bound: u32, upper_bound: u32, val: f64) -> f64 {
        let upper_bound = upper_bound as f64;
        let lower_bound = lower_bound as f64;
        let rel_val = ((upper_bound - lower_bound) * val) + lower_bound;
        rel_val.ceil().min(upper_bound).max(lower_bound)
    }

    pub fn contained(&self, lower_bound: u32, upper_bound: u32) -> Self {
        Self {
            red: Self::contain_val(lower_bound, upper_bound, self.red),
            green: Self::contain_val(lower_bound, upper_bound, self.green),
            blue: Self::contain_val(lower_bound, upper_bound, self.blue),
        }
    }
}

impl std::cmp::PartialEq for Color {
    fn eq(&self, other: &Self) -> bool {
        compare_floats(self.red, other.red)
            && compare_floats(self.green, other.green)
            && compare_floats(self.blue, other.blue)
    }
}

impl std::ops::Add for &Color {
    type Output = Color;

    fn add(self, other: Self) -> Self::Output {
        Self::Output::new(
            self.red + other.red,
            self.green + other.green,
            self.blue + other.blue,
        )
    }
}

impl std::ops::Sub for &Color {
    type Output = Color;

    fn sub(self, other: Self) -> Self::Output {
        Self::Output::new(
            self.red - other.red,
            self.green - other.green,
            self.blue - other.blue,
        )
    }
}

impl std::ops::Mul for &Color {
    type Output = Color;

    fn mul(self, other: Self) -> Self::Output {
        Self::Output::new(
            self.red * other.red,
            self.green * other.green,
            self.blue * other.blue,
        )
    }
}

impl std::ops::Mul<f64> for &Color {
    type Output = Color;

    fn mul(self, scalar: f64) -> Self::Output {
        Self::Output::new(scalar * self.red, scalar * self.green, scalar * self.blue)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::assert_eqf;

    #[test]
    fn color_creation() {
        let color = Color::new(-0.5, 0.4, 1.7);

        assert_eqf!(color.red, -0.5);
        assert_eqf!(color.green, 0.4);
        assert_eqf!(color.blue, 1.7);
    }

    #[test]
    fn color_equality() {
        let color = Color {
            red: 1.0,
            green: 2.0,
            blue: 3.0,
        };

        assert_eq!(
            color,
            Color {
                red: 1.0,
                green: 2.0,
                blue: 3.0
            }
        );
        assert_ne!(
            color,
            Color {
                red: 1.1,
                green: 2.0,
                blue: 3.0
            }
        );
        assert_ne!(
            color,
            Color {
                red: 1.0,
                green: 2.1,
                blue: 3.0
            }
        );
        assert_ne!(
            color,
            Color {
                red: 1.0,
                green: 2.0,
                blue: 3.1
            }
        );
    }

    #[test]
    fn color_addition() {
        let color1 = &Color::new(0.9, 0.6, 0.75);
        let color2 = &Color::new(0.7, 0.1, 0.25);

        assert_eq!(
            color1 + color2,
            Color {
                red: 1.6,
                green: 0.7,
                blue: 1.0
            }
        );
    }

    #[test]
    fn color_subtraction() {
        let color1 = &Color::new(0.9, 0.6, 0.75);
        let color2 = &Color::new(0.7, 0.1, 0.20);

        assert_eq!(
            color1 - color2,
            Color {
                red: 0.2,
                green: 0.5,
                blue: 0.55
            }
        );
    }

    #[test]
    fn color_scalar_multiplication() {
        let color = &Color::new(0.2, 0.3, 0.4);

        assert_eq!(
            color * 2.0,
            Color {
                red: 0.4,
                green: 0.6,
                blue: 0.8
            }
        );
    }

    #[test]
    fn color_multiplication() {
        let color1 = &Color::new(1.0, 0.2, 0.4);
        let color2 = &Color::new(0.9, 1.0, 0.1);

        assert_eq!(
            color1 * color2,
            Color {
                red: 0.9,
                green: 0.2,
                blue: 0.04,
            }
        )
    }

    #[test]
    fn color_containment() {
        let color1 = Color::new(1.5, 0.0, 0.0);
        let color2 = Color::new(0.0, 0.5, 0.0);
        let color3 = Color::new(-0.5, 0.0, 1.0);

        assert_eq!(
            color1.contained(0, 255),
            Color {
                red: 255.0,
                green: 0.0,
                blue: 0.0
            }
        );
        assert_eq!(
            color2.contained(0, 255),
            Color {
                red: 0.0,
                green: 128.0,
                blue: 0.0
            }
        );
        assert_eq!(
            color3.contained(0, 255),
            Color {
                red: 0.0,
                green: 0.0,
                blue: 255.0
            }
        );
    }
}
