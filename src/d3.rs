use crate::math::compare_floats;
use crate::matrix;

#[derive(Debug, Clone)]
pub struct Point3D {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

#[derive(Debug, Clone)]
pub struct Vector3D {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Point3D {
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Self { x, y, z }
    }

    fn from_row_of_4(row: &matrix::RowOf4) -> Self {
        match compare_floats(row[3], 1.0) {
            true => Self::new(row[0], row[1], row[2]),
            false => panic!(
                "cannot convert row representing a vector to Point3D: {:?}",
                row
            ),
        }
    }

    fn as_row_of_4(&self) -> matrix::RowOf4 {
        matrix::RowOf4::new(self.x, self.y, self.z, 1.0)
    }
}

impl Vector3D {
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Self { x, y, z }
    }

    pub fn from_row_of_4(row: &matrix::RowOf4) -> Self {
        match compare_floats(row[3], 0.0) {
            true => Self::new(row[0], row[1], row[2]),
            false => panic!(
                "cannot convert row representing a point to Vector3D: {:?}",
                row
            ),
        }
    }

    pub fn abs(&self) -> f64 {
        (self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt()
    }

    pub fn normalized(&self) -> Vector3D {
        self / self.abs()
    }

    pub fn dot(&self, other: &Self) -> f64 {
        (self.x * other.x) + (self.y * other.y) + (self.z * other.z)
    }

    pub fn cross(&self, other: &Self) -> Self {
        Self {
            x: (self.y * other.z) - (self.z * other.y),
            y: (self.z * other.x) - (self.x * other.z),
            z: (self.x * other.y) - (self.y * other.x),
        }
    }

    pub fn as_row_of_4(&self) -> matrix::RowOf4 {
        matrix::RowOf4::new(self.x, self.y, self.z, 0.0)
    }

    pub fn reflect_around(&self, normal: &Self) -> Self {
        self - &(normal * (2.0 * self.dot(normal)))
    }
}

impl PartialEq for Point3D {
    fn eq(&self, other: &Self) -> bool {
        compare_floats(self.x, other.x)
            && compare_floats(self.y, other.y)
            && compare_floats(self.z, other.z)
    }
}

impl PartialEq<Vector3D> for Point3D {
    fn eq(&self, _: &Vector3D) -> bool {
        false
    }
}

impl std::ops::Add<&Vector3D> for &Point3D {
    type Output = Point3D;

    fn add(self, vector: &Vector3D) -> Self::Output {
        Self::Output {
            x: self.x + vector.x,
            y: self.y + vector.y,
            z: self.z + vector.z,
        }
    }
}

impl std::ops::Sub for &Point3D {
    type Output = Vector3D;

    fn sub(self, other: Self) -> Self::Output {
        Self::Output {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl std::ops::Sub<&Vector3D> for &Point3D {
    type Output = Point3D;

    fn sub(self, vector: &Vector3D) -> Self::Output {
        Self::Output {
            x: self.x - vector.x,
            y: self.y - vector.y,
            z: self.z - vector.z,
        }
    }
}

impl PartialEq for Vector3D {
    fn eq(&self, other: &Self) -> bool {
        compare_floats(self.x, other.x)
            && compare_floats(self.y, other.y)
            && compare_floats(self.z, other.z)
    }
}

impl PartialEq<Point3D> for Vector3D {
    fn eq(&self, _: &Point3D) -> bool {
        false
    }
}

impl std::ops::Add<&Point3D> for &Vector3D {
    type Output = Point3D;

    fn add(self, point: &Point3D) -> Self::Output {
        Self::Output {
            x: self.x + point.x,
            y: self.y + point.y,
            z: self.z + point.z,
        }
    }
}

impl std::ops::Sub for &Vector3D {
    type Output = Vector3D;

    fn sub(self, other: Self) -> Self::Output {
        Self::Output {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl std::ops::Neg for &Vector3D {
    type Output = Vector3D;

    fn neg(self) -> Self::Output {
        Self::Output {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl std::ops::Mul<f64> for &Vector3D {
    type Output = Vector3D;

    fn mul(self, scalar: f64) -> Self::Output {
        Self::Output {
            x: self.x * scalar,
            y: self.y * scalar,
            z: self.z * scalar,
        }
    }
}

impl std::ops::Div<f64> for &Vector3D {
    type Output = Vector3D;

    fn div(self, scalar: f64) -> Self::Output {
        Self::Output {
            x: self.x / scalar,
            y: self.y / scalar,
            z: self.z / scalar,
        }
    }
}

impl std::ops::Mul<&Vector3D> for &matrix::Matrix4By4 {
    type Output = Vector3D;

    fn mul(self, vector: &Vector3D) -> Self::Output {
        Self::Output::from_row_of_4(&(self * &vector.as_row_of_4()))
    }
}

impl std::ops::Mul<&Point3D> for &matrix::Matrix4By4 {
    type Output = Point3D;

    fn mul(self, point: &Point3D) -> Self::Output {
        Self::Output::from_row_of_4(&(self * &point.as_row_of_4()))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::assert_eqf;
    use std::f64::consts::PI;
    use std::panic::catch_unwind;

    #[test]
    fn point_equality() {
        let point = Point3D {
            x: 1.0,
            y: 2.0,
            z: 3.0,
        };

        assert_eq!(
            point,
            Point3D {
                x: 1.0,
                y: 2.0,
                z: 3.0
            }
        );
        assert_ne!(
            point,
            Point3D {
                x: 1.1,
                y: 2.0,
                z: 3.0
            }
        );
        assert_ne!(
            point,
            Point3D {
                x: 1.0,
                y: 2.1,
                z: 3.0
            }
        );
        assert_ne!(
            point,
            Point3D {
                x: 1.0,
                y: 2.0,
                z: 3.1
            }
        );
    }

    #[test]
    fn vector_equality() {
        let vector = Vector3D {
            x: 1.0,
            y: 2.0,
            z: 3.0,
        };

        assert_eq!(
            vector,
            Vector3D {
                x: 1.0,
                y: 2.0,
                z: 3.0
            }
        );
        assert_ne!(
            vector,
            Vector3D {
                x: 1.1,
                y: 2.0,
                z: 3.0
            }
        );
        assert_ne!(
            vector,
            Vector3D {
                x: 1.0,
                y: 2.1,
                z: 3.0
            }
        );
        assert_ne!(
            vector,
            Vector3D {
                x: 1.0,
                y: 2.0,
                z: 3.1
            }
        );
    }

    #[test]
    fn point_creation() {
        let point = Point3D::new(1.0, 2.0, 3.0);

        assert_eqf!(point.x, 1.0);
        assert_eqf!(point.y, 2.0);
        assert_eqf!(point.z, 3.0);
    }

    #[test]
    fn vector_creation() {
        let vector = Vector3D::new(1.0, 2.0, 3.0);

        assert_eqf!(vector.x, 1.0);
        assert_eqf!(vector.y, 2.0);
        assert_eqf!(vector.z, 3.0);
    }

    #[test]
    fn adding_vector_to_point_gives_point() {
        let point = Point3D::new(3.0, -2.0, 5.0);
        let vector = Vector3D::new(-2.0, 3.0, 1.0);

        assert_eq!(
            &point + &vector,
            Point3D {
                x: 1.0,
                y: 1.0,
                z: 6.0
            }
        );
    }

    #[test]
    fn adding_point_to_vector_gives_point() {
        let point = Point3D::new(3.0, -2.0, 5.0);
        let vector = Vector3D::new(-2.0, 3.0, 1.0);

        assert_eq!(
            &vector + &point,
            Point3D {
                x: 1.0,
                y: 1.0,
                z: 6.0
            }
        );
    }

    #[test]
    fn subtracting_two_points_gives_vector() {
        let point1 = Point3D::new(3.0, 2.0, 1.0);
        let point2 = Point3D::new(5.0, 6.0, 7.0);

        assert_eq!(
            &point1 - &point2,
            Vector3D {
                x: -2.0,
                y: -4.0,
                z: -6.0
            }
        );
    }

    #[test]
    fn subtracting_vector_from_point_gives_point() {
        let point = Point3D::new(3.0, 2.0, 1.0);
        let vector = Vector3D::new(5.0, 6.0, 7.0);

        assert_eq!(
            &point - &vector,
            Point3D {
                x: -2.0,
                y: -4.0,
                z: -6.0
            }
        );
    }

    #[test]
    fn subtracting_two_vectors_gives_vector() {
        let vector1 = Vector3D::new(3.0, 2.0, 1.0);
        let vector2 = Vector3D::new(5.0, 6.0, 7.0);

        assert_eq!(
            &vector1 - &vector2,
            Vector3D {
                x: -2.0,
                y: -4.0,
                z: -6.0
            }
        );
    }

    #[test]
    fn vector_negation() {
        let vector = Vector3D::new(1.0, -2.0, 3.0);

        assert_eq!(
            -&vector,
            Vector3D {
                x: -1.0,
                y: 2.0,
                z: -3.0
            }
        );
    }

    #[test]
    fn vector_scalar_multiplication() {
        let vector = Vector3D::new(1.0, -2.0, 3.0);

        assert_eq!(
            &vector * 3.5,
            Vector3D {
                x: 3.5,
                y: -7.0,
                z: 10.5
            }
        );
        assert_eq!(
            &vector * 0.5,
            Vector3D {
                x: 0.5,
                y: -1.0,
                z: 1.5
            }
        );
    }

    #[test]
    fn vector_scalar_division() {
        let vector = Vector3D::new(1.0, -2.0, 3.0);

        assert_eq!(
            &vector / 2.0,
            Vector3D {
                x: 0.5,
                y: -1.0,
                z: 1.5
            }
        );
    }

    #[test]
    fn vector_magnitude() {
        let vector1 = Vector3D::new(1.0, 0.0, 0.0);
        let vector2 = Vector3D::new(0.0, 1.0, 0.0);
        let vector3 = Vector3D::new(0.0, 0.0, 1.0);
        let vector4 = Vector3D::new(1.0, 2.0, 3.0);
        let vector5 = Vector3D::new(-1.0, -2.0, -3.0);

        assert_eqf!(vector1.abs(), 1.0);
        assert_eqf!(vector2.abs(), 1.0);
        assert_eqf!(vector3.abs(), 1.0);
        assert_eqf!(vector4.abs(), 14_f64.sqrt());
        assert_eqf!(vector5.abs(), 14_f64.sqrt());
    }

    #[test]
    fn vector_normalization() {
        let vector1 = Vector3D::new(4.0, 0.0, 0.0);
        let vector2 = Vector3D::new(1.0, 2.0, 3.0);

        let norm1 = vector1.normalized();
        let norm2 = vector2.normalized();

        assert_eq!(
            norm1,
            Vector3D {
                x: 1.0,
                y: 0.0,
                z: 0.0
            }
        );
        assert_eq!(
            norm2,
            Vector3D {
                x: 0.26726,
                y: 0.53452,
                z: 0.80178
            }
        );
        assert_eqf!(norm1.abs(), 1.0);
        assert_eqf!(norm2.abs(), 1.0);
    }

    #[test]
    fn dot_product() {
        let vector1 = Vector3D::new(1.0, 2.0, 3.0);
        let vector2 = Vector3D::new(2.0, 3.0, 4.0);

        assert_eqf!(vector1.dot(&vector2), 20.0);
    }

    #[test]
    fn cross_product() {
        let vector1 = Vector3D::new(1.0, 2.0, 3.0);
        let vector2 = Vector3D::new(2.0, 3.0, 4.0);

        assert_eq!(
            vector1.cross(&vector2),
            Vector3D {
                x: -1.0,
                y: 2.0,
                z: -1.0,
            }
        );
        assert_eq!(
            vector2.cross(&vector1),
            Vector3D {
                x: 1.0,
                y: -2.0,
                z: 1.0,
            }
        );
    }

    #[test]
    fn point_as_row_of_4() {
        let vector1 = Point3D::new(0.0, 0.0, 0.0);
        let vector2 = Point3D::new(-2.0, 3.0, -4.0);

        assert_eq!(
            vector1.as_row_of_4(),
            matrix::RowOf4::new(0.0, 0.0, 0.0, 1.0)
        );
        assert_eq!(
            vector2.as_row_of_4(),
            matrix::RowOf4::new(-2.0, 3.0, -4.0, 1.0)
        );
    }

    #[test]
    fn point_from_row_of_4() {
        let point_row = matrix::RowOf4::new(5.0, 8.0, 13.0, 1.0);
        let vec_row = matrix::RowOf4::new(5.0, 8.0, 13.0, 0.0);

        assert_eq!(
            Point3D::from_row_of_4(&point_row),
            Point3D::new(5.0, 8.0, 13.0)
        );
        let res = catch_unwind(|| Point3D::from_row_of_4(&vec_row));
        assert!(res.is_err());
    }

    #[test]
    fn vector_as_row_of_4() {
        let vector1 = Vector3D::new(0.0, 0.0, 0.0);
        let vector2 = Vector3D::new(-2.0, 3.0, -4.0);

        assert_eq!(
            vector1.as_row_of_4(),
            matrix::RowOf4::new(0.0, 0.0, 0.0, 0.0)
        );
        assert_eq!(
            vector2.as_row_of_4(),
            matrix::RowOf4::new(-2.0, 3.0, -4.0, 0.0)
        );
    }

    #[test]
    fn vector_from_row_of_4() {
        let vec_row = matrix::RowOf4::new(5.0, 8.0, 13.0, 0.0);
        let point_row = matrix::RowOf4::new(5.0, 8.0, 13.0, 1.0);

        assert_eq!(
            Vector3D::from_row_of_4(&vec_row),
            Vector3D::new(5.0, 8.0, 13.0)
        );
        let res = catch_unwind(|| Vector3D::from_row_of_4(&point_row));
        assert!(res.is_err());
    }

    #[test]
    fn forward_translation_of_point_by_matrix() {
        let point = Point3D::new(-3.0, 4.0, 5.0);
        let transform = matrix::Matrix4By4::for_translating(5.0, -3.0, 2.0);

        assert_eq!(&transform * &point, Point3D::new(2.0, 1.0, 7.0));
    }

    #[test]
    fn backward_translation_of_point_by_matrix() {
        let point = Point3D::new(-3.0, 4.0, 5.0);
        let transform = matrix::Matrix4By4::for_translating(5.0, -3.0, 2.0);
        let inv = transform.inverted();

        assert_eq!(&inv * &point, Point3D::new(-8.0, 7.0, 3.0));
    }

    #[test]
    fn non_translation_of_vectors() {
        let vector = Vector3D::new(-3.0, 4.0, 5.0);
        let transform = matrix::Matrix4By4::for_translating(5.0, -3.0, 2.0);

        assert_eq!(&transform * &vector, Vector3D::new(-3.0, 4.0, 5.0));
    }

    #[test]
    fn scaling_of_points_by_matrix() {
        let point = Point3D::new(-4.0, 6.0, 8.0);
        let transform = matrix::Matrix4By4::for_scaling(2.0, 3.0, 4.0);

        assert_eq!(&transform * &point, Point3D::new(-8.0, 18.0, 32.0));
    }

    #[test]
    fn scaling_of_vectors_by_matrix() {
        let vector = Vector3D::new(-4.0, 6.0, 8.0);
        let transform = matrix::Matrix4By4::for_scaling(2.0, 3.0, 4.0);

        assert_eq!(&transform * &vector, Vector3D::new(-8.0, 18.0, 32.0));
    }

    #[test]
    fn shrinking_of_points_and_vectors() {
        let point = Point3D::new(-4.0, 15.0, 16.0);
        let vector = Vector3D::new(-4.0, 15.0, 16.0);
        let transform = matrix::Matrix4By4::for_scaling(2.0, 3.0, 4.0);
        let shrinker = transform.inverted();

        assert_eq!(&shrinker * &point, Point3D::new(-2.0, 5.0, 4.0));
        assert_eq!(&shrinker * &vector, Vector3D::new(-2.0, 5.0, 4.0));
    }

    #[test]
    fn reflection_of_points_and_vectors() {
        let point = Point3D::new(2.0, 3.0, 4.0);
        let vector = Vector3D::new(2.0, 3.0, 4.0);
        let transform = matrix::Matrix4By4::for_scaling(-1.0, 1.0, 1.0);

        assert_eq!(&transform * &point, Point3D::new(-2.0, 3.0, 4.0));
        assert_eq!(&transform * &vector, Vector3D::new(-2.0, 3.0, 4.0));
    }

    #[test]
    fn rotation_of_points() {
        let point = Point3D::new(0.0, 1.0, 0.0);
        let half_quarter_radians = PI / 4.0;
        let full_quarter_radians = PI / 2.0;

        // x rotation
        assert_eq!(
            &matrix::Matrix4By4::for_x_rotation(half_quarter_radians) * &point,
            Point3D::new(0.0, 2f64.sqrt() / 2.0, 2f64.sqrt() / 2.0)
        );
        assert_eq!(
            &matrix::Matrix4By4::for_x_rotation(full_quarter_radians) * &point,
            Point3D::new(0.0, 0.0, 1.0)
        );

        let point_y = Point3D::new(0.0, 0.0, 1.0);
        assert_eq!(
            &matrix::Matrix4By4::for_y_rotation(half_quarter_radians) * &point_y,
            Point3D::new(2f64.sqrt() / 2.0, 0.0, 2f64.sqrt() / 2.0)
        );
        assert_eq!(
            &matrix::Matrix4By4::for_y_rotation(full_quarter_radians) * &point_y,
            Point3D::new(1.0, 0.0, 0.0)
        );

        // z rotation
        assert_eq!(
            &matrix::Matrix4By4::for_z_rotation(half_quarter_radians) * &point,
            Point3D::new(-2f64.sqrt() / 2.0, 2f64.sqrt() / 2.0, 0.0)
        );
        assert_eq!(
            &matrix::Matrix4By4::for_z_rotation(full_quarter_radians) * &point,
            Point3D::new(-1.0, 0.0, 0.0)
        );
    }
}
