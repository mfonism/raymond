use crate::color::Color;
use crate::d3::Point3D;

#[derive(Debug)]
pub struct PointLight {
    pub position: Point3D,
    pub intensity: Color,
}

impl PointLight {
    pub fn new(position: Point3D, intensity: Color) -> Self {
        Self {
            position,
            intensity,
        }
    }
}

impl std::cmp::PartialEq for PointLight {
    fn eq(&self, other: &Self) -> bool {
        self.position == other.position && self.intensity == other.intensity
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn creating_a_point_light() {
        let intensity = Color::new(1.0, 1.0, 1.0);
        let position = Point3D::new(0.0, 0.0, 0.0);

        let light = PointLight::new(position, intensity);

        assert_eq!(light.position, Point3D::new(0.0, 0.0, 0.0));
        assert_eq!(light.intensity, Color::new(1.0, 1.0, 1.0));
    }
}
