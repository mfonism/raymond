# Raymond

## The What

A Rust powered ray tracer library built by walking through Jamis Buck's wonderful book [The Ray Tracer Challenge](https://pragprog.com/titles/jbtracer/the-ray-tracer-challenge/).

## The Why

I'm four weeks into learning Rust in my spare time, and I'm taking this challenge to improve my Rust knowledge and skills.

## The Journey

I'm documenting my progress in my [Journey Book](./JOURNEY_BOOK.md)
