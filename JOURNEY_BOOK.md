# Day 01

_Monday_, __November 01, 2021__

## Chapter 01: Tuples, Points and Vectors

### The Journey

The book asked to represent points and vectors as tuples with four members, labelled `x`, `y`, `z`, and `w`.

For points, I understand `x`, `y` and `z` represent the coordinates of the point in 3D space. For vectors, I understand they represent the coordinates of the tip of a vector shooting out of __origin__, which has the same magnitude and direction as the vector being represented by the tuple.

The `w` member holds a boolean flag which says whether the tuple encodes a vector (w ::= 0) or a point (w ::= 1).

I started by implementing them as variants, `Point` and `Vector`, of an enum, `Tuple`. But when I started implementing operations on them it became clear to me that they would be best implemented as structs. And so I switched to structs.

### Things Learnt

* I learnt how to implement traits for structs.

  I was especially excited to _discover_ traits with generic type parameters. My world was expanded by this _discovery_ and I was able to implement operators with different types on their LHS and RHS &mdash; like subtraction of vectors from points, multiplication of vectors by scalar quantities, and division of vectors by scalar quantities.

* I learnt how to write tests in Rust.

  Conventionally, unittest modules in Rust are structured in a different way than I'm used to. At first, it felt a little awkward to write them in the same file containing the code they were to test. But I looked around and read about module hierarchy in Rust. This made it clear to me that although the tests were in the same file as the production code, they were nested in a module under the production code module.

* I learnt how to structure files to create crates and modules in my project.

  Although I haven't totally internalized this, I'm signing up for a code review this Friday at the Recurse Center (where I'm currently studying) and asking that the review leans towards code organization.

* I wrote a macro to assert _approximate_ equivalence of floating point numbers in tests.

  This way, I no longer have to use the builtin `assert_eq!` macro to test float equivalence. Long live `assert_eqf!` &mdash; the naming was inspired by `powf` and `powi` in the standard library, which I _discovered_ when trying to raise my floats to powers.

* I refreshed my knowledge on vectors and took extra readings that helped me recapture intuition about dot products, cross products, and vector normalization.


# Day 02

_Tuesday_, __November 02, 2021__

## Chapter 02: Drawing on Canvas

### The Journey

The book asked to create color data as _vectors_ of color points, implement addition, subtraction and multiplication operations on them, and to implement a canvas for _painting_ the colors on.

I implemented the essence of the canvas as a hashmap whose keys are combinations of the x and y coordinates of its pixels, and the values are the colors of the respective pixels. I made a decision to lean towards memory efficiency by adding entries for only non-black colored pixels to the hashmap. This way, any valid (x,y) combination that isn't in the hashmap is seen as a black colored pixel.

The duty of producing all the pixels (including the black colored ones) was delegated for when the canvas has to render itself.

### Things Learnt

* I captured intuition about the type signature for a number of methods on HashMap.

* It became clear to me that because a container type like a hashmap has to own its contents, the contents have to be __moved__ into insertion methods, and __borrowed__ by inspection methods.

* I may have captured intuition for how the ops traits work. I believe I no longer need to implement them for `&T`, but can get away with implementing them only for `T` and specifying the `Rhs` type as `&T`.


# Day 03

_Wednesday_, __November 03, 2021__

## Chapter 03: Matrices

### The Journey

This chapter brought back memories of doing matrix calculus back in highschool. In working with matrices here I internalised the (previously held by rote) knowledge that matrix multiplication is not commutative, and that order matters.

This helped resolve long standing confusion about some things. Like why `A * B = C` meant `B = Ainv * C` and didn't also mean `B = C * Ainv` as one would expect when using intuition from multiplication of numbers.

```
A * B = C
Ainv * (A * B) = Ainv * C  // Ainv is inserted into the same spot on both sides
(Ainv * A) * B = Ainv * C
I * B = Ainv * C
B = Ainv * C
```

Really, this is an important realization for me as things like these which I had not yet intuitively grasped used to keep me awake at night in my highschool days.


# Day 04

_Thursday_, __November 04, 2021__

## Chapter 03: Matrices

### The Journey

The book asked to implement matrix operations like identity, transposition, inversion. I corrected something I'd previously learnt wrongly on day 2.

### Things Learnt

* I made a realization that helped me correct belief from day 2 that I can get away by implementing mathematical operations only for `T` and specifying the `Rhs` type as `&T`. 

  Specifically I saw that a value of type `T` will always be moved into the those operations (regardless of whether it was on the RHS or the LHS) and will no longer be available after the respective operation unless they were a `Copy` type. My matrix type isn't a copy type, and so I have to implement the operations to work on references to matrices.
  
* I learnt that when Index and IndexMut are implemented for a type T:

  + the expression `t[idx]` desugars to `*t.index(idx)` and
  + the expression `t[idx] = k` desugars to `*t.index_mut(idx) = k`.
  
  I wasn't expecting that dereferencing, TBH. It threw me off for a minute there when I couldn't use the resulting values as references.


# Day 05

_Friday_, __November 05, 2021__

## Chapter 04: Matrix Transformation

### The Journey

Working through matrix transofrmations helped me realize the power of matrix-vector multiplication.

For the first time I was able to appreciate the role of each element in the multiplicand matrix. I discovered (for myself) that a matrix is really equivalent to the LHS of a simultaneous equation in the conventional form.

I googled around and was excited to see that it indeed models a system of linear equations.

And now I know just how to solve a system of simultaneous equations with MAAAAAAAATRICES.

Like, shove me into a closet and shut the door! I'm so excited, the people around me are about to witness someone glitch in real time.


# Day 05

_(Part Two, because I glitched... and had to be shoved into a closet.)_

_Friday_, __November 05, 2021__

## Chapter 04: Matrix Transformation

### The Journey

The book asked to implement a number of transformers for matrices. Good thing I had implemented the `Index` and `IndexMut` traits on my matrix type. Also, good thing I now have an intuitive understanding of the role of matrix elements in multiplication.

I implemented these matrix transformers by starting with the identity matrix, and then setting the contents at specific indices to values that ensure the matrices have the respective transformation effects when used as multiplicands.

### Things Learnt

* To trust my guts.

  No, really!

  I went with my first idea when I came to the optional closing exercise for the chapter, and it worked perfectly.

* To be careful when casting between number types, lest you lose precison.

  ~When computing angles for rotation in the closing exercise, I did stuff like `((1 / 6) * PI) as f32`, and, while I can't now imagine why I ever did that, it was a thorn on my side for a long enough time.~

  *EDIT:* It's so super unfortunate I didn't correctly document what I did wrong. So I've lost that example. I feel so terrible about that, but the lesson remains. Also, there's a lesson here about documenting wrong solutions correctly.


# Day 06

_Monday_, __November 08, 2021__

## Chapter 05: Ray-sphere intersection

### The Journey

The book demonstrated how to figure out which rays hit which objects in a world. This helped me immediately recognize how we we're going to paint objects on the canvas. For a scene containing one light source and one object, we:
* position the object in between the light source and the canvas
* loop over all the pixels/cells in the canvas, tracing a ray from the source to each pixel on the canvas:
    + color pixels on the canvas for which the ray hits the object

It felt so good to realize this myself.

### Things Learnt

* I was curious about the mismatch between how I define PartialEq and how I have to actually use the `==` operator. It was radically different from what was the case with arithmetic operators (`*`, `/`, `+`, `-`). So, I dug around and found that [the compiler actually translates comparison and arithmetic operators differently](https://stackoverflow.com/a/54101468/11413244) in this manner:

 ```rust
 a == b   ==>   std::cmp::PartialEq::eq(&a, &b)
 a + b    ==>   std::ops::Add::add(a, b)
 ```

 And this clears things up for me.

* I internalized that `&[T]` is not an array, and learnt what it really is. And as a bonus I learnt why it is better to use `&[T]` the argument type to functions &mdash; as opposed to &String or `&Vec`. Or `&Box` for that matter, although I don't know what that one is... yet.
